/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Jeff Backus <jeff@jsbackus.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*! \file
 * Unit tests for SCOOT.
 */

#include <scoot.h>
#include <limits.h>

START_SUITE( BasicTestsC )

TEST( TestPass ) {

  int a = 1;
  int b = 1;

  ASSERT( a == b );

  TEST_PASS
}

TEST( TestFail ) {

  int a = 1;
  int b = 2;

  ASSERT( a == b );

  TEST_PASS
}

TEST( AssertEqualsPass ) {

  int a = 3;
  int b = 3;

  ASSERT_EQUALS( a, b );

  TEST_PASS
}

TEST( AssertEqualsFail ) {

  int a = 3;
  int b = 9;

  ASSERT_EQUALS( a, b );

  TEST_PASS
}

TEST( AssertNotEqualsPass ) {

  int a = 3;
  int b = 7;

  ASSERT_NOT_EQUALS( a, b );

  TEST_PASS
}

TEST( AssertNotEqualsFail ) {

  int a = 9;
  int b = 9;

  ASSERT_NOT_EQUALS( a, b );

  TEST_PASS
}

TEST( AssertLessThanPass ) {

  int a = 3;
  int b = 4;

  ASSERT_LESS_THAN( a, b );

  TEST_PASS
}

TEST( AssertLessThanFail_GreaterCheck ) {

  int a = 79;
  int b = 78;

  ASSERT_LESS_THAN( a, b );

  TEST_PASS
}

TEST( AssertLessThanFail_EqualCheck ) {

  int a = 4;
  int b = 4;

  ASSERT_LESS_THAN( a, b );

  TEST_PASS
}

TEST( AssertLessThanEqualsPass ) {

  int a = 3;
  int b = 4;
  int c = 3;

  ASSERT_LESS_THAN_EQUALS( a, b );

  ASSERT_LESS_THAN_EQUALS( a, c );
  
  TEST_PASS
}

TEST( AssertLessThanEqualsFail ) {

  int a = 9;
  int b = 7;

  ASSERT_LESS_THAN_EQUALS( a, b );

  TEST_PASS
}

TEST( AssertGreaterThanPass ) {

  int a = 173;
  int b = 171;

  ASSERT_GREATER_THAN( a, b );

  TEST_PASS
}

TEST( AssertGreaterThanFail_LessCheck ) {

  int a = 17;
  int b = 19;

  ASSERT_GREATER_THAN( a, b );

  TEST_PASS
}

TEST( AssertGreaterThanFail_EqualCheck ) {

  int a = 42;
  int b = 42;

  ASSERT_GREATER_THAN( a, b );

  TEST_PASS
}

TEST( AssertGreaterThanEqualsPass ) {

  int a = 35;
  int b = 34;
  int c = 35;

  ASSERT_GREATER_THAN_EQUALS( a, b );

  ASSERT_GREATER_THAN_EQUALS( a, c );
  
  TEST_PASS
}

TEST( AssertGreaterThanEqualsFail ) {

  int a = 1;
  int b = 2;

  ASSERT_GREATER_THAN_EQUALS( a, b );

  TEST_PASS
}

END_SUITE
