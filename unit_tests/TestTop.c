/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Jeff Backus <jeff@jsbackus.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*! \file
 * Top-level test suite
 */

#include <scoot.h>
#include "mylib.h"

START_SUITE( TestTop )

SUITE_SETUP {

  int c = count();
  ASSERT( c == 1 );

  resetCounter();
  
  TEST_PASS
}

/* Defined in BasicSuite.c */
ADD_SUITE( BasicSuite )

/* Defined in Subsuites.c */
ADD_SUITE( Subsuites )

TEST_WITH_ID( CyclicPre, 100 ) {

  resetCounter();
  
  ASSERT( count() == 1 );

  TEST_PASS
}

/* Defined in CyclicDependencies.c */
ADD_SUITE_WITH_ID( CyclicDependencies, 100 )

TEST_WITH_ID( CyclicPost, 102 ) {

  int c = count();
  ASSERT( c == 3 );

  TEST_PASS
}

/* Defined in TestOrder.c */
ADD_SUITE( TestOrder )

/* Defined in TestDefaultFns.c */
ADD_SUITE( TestDefaultFns )

/* Defined in SuiteOrder.c */
ADD_SUITE( SuiteOrder )

/* Defined in SuiteSetupFail.c */
ADD_SUITE( SuiteSetupFail )

/* Defined in SuiteSetupFail.c */
ADD_SUITE( SuiteCleanupFail )

/* Defined in MisnamedSuite.c */
ADD_SUITE( Misnamed_Suite )

/* Defined in TestLogLevel.c */
ADD_SUITE_WITH_ID( TestLogLevel, 1000 )

END_SUITE
