/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Jeff Backus <jeff@jsbackus.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*! \file
 * Unit tests related order of tests
 */

#include <scoot.h>

/* Generally bad juju to encourage interaction between tests, but it is the
   easiest way to verify order. */
int _count = 0;

START_SUITE( TestDefaultFns )

SETUP( TestBothSpecified ) {

  _count++;
  
  ASSERT( _count == 1 );

  TEST_PASS
}

TEST_WITH_ID( TestBothSpecified, 1 ) {

  _count++;
  
  ASSERT( _count == 2 );

  TEST_PASS
}

CLEANUP( TestBothSpecified ) {

  _count++;
  
  ASSERT( _count == 3 );

  TEST_PASS
}

TEST_WITH_ID( TestDefaultSetupOnly, 2 ) {

  _count++;
  
  ASSERT( _count == 5 );

  TEST_PASS
}

CLEANUP( TestDefaultSetupOnly ) {

  _count++;
  
  ASSERT( _count == 6 );

  TEST_PASS
}

DEFAULT_CLEANUP {
  _count++;
  
  TEST_PASS
}

DEFAULT_SETUP {
  _count++;

  TEST_PASS
}

SETUP( TestDefaultCleanupOnly ) {

  _count++;
  
  ASSERT( _count == 7 );

  TEST_PASS
}

TEST_WITH_ID( TestDefaultCleanupOnly, 3 ) {

  _count++;
  
  ASSERT( _count == 8 );

  TEST_PASS
}

TEST_WITH_ID( TestDefaultBoth, 4 ) {

  _count++;
  
  ASSERT( _count == 11 );

  TEST_PASS
}
