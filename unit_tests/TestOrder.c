/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Jeff Backus <jeff@jsbackus.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*! \file
 * Unit tests related order of tests
 */

#include <scoot.h>

/* Generally bad juju to encourage interaction between tests, but it is the
   easiest way to verify order. */
int _count = 0;

START_SUITE( TestOrder )

SUITE_CLEANUP {
  _count++;
  
  ASSERT( _count == 32 );
  
  TEST_PASS
}

SUITE_SETUP {
  _count++;

  ASSERT( _count == 1 );
  
  TEST_PASS
}

SETUP( TestOrderR ) {

  _count++;

  ASSERT( (_count == 8) || (_count == 11) );

  TEST_PASS
}

TEST_WITH_ID( TestOrderA, 0 ) {

  _count++;
  
  ASSERT( _count == 3 );

  TEST_PASS
}

CLEANUP( TestOrderH ) {

  _count++;
  
  ASSERT( _count == 25 );

  TEST_PASS
}

CLEANUP( TestOrderV ) {

  _count++;
  
  ASSERT( (_count == 31) || (_count == 28) );

  TEST_PASS
}

CLEANUP( TestOrderL ) {

  _count++;
  
  ASSERT( (_count == 15) || (_count == 10) );

  TEST_PASS
}

TEST_WITH_ID( TestOrderB, 0 ) {

  _count++;
  
  ASSERT( _count == 6 );

  TEST_PASS
}

CLEANUP( UnmatchedA ) {
  /* Should never run */
  _count++;

  ASSERT( _count == 6 );

  TEST_PASS
}

TEST_WITH_ID( TestOrderR, 1 ) {

  _count++;
  
  ASSERT( (_count == 9) || (_count == 12) );

  TEST_PASS
}

SETUP( TestOrderG ) {

  _count++;
  
  ASSERT( (_count == 10) || (_count == 13) );

  TEST_PASS
}

SETUP( TestOrderV ) {

  _count++;

  ASSERT( (_count == 29) || (_count == 26) );

  TEST_PASS
}

SETUP( UnmatchedA ) {
  /* Should never run */
  _count++;

  ASSERT( _count == 6 );

  TEST_PASS
}

TEST_WITH_ID( TestOrderH, 1000 ) {

  _count++;
  
  ASSERT( _count == 24 );

  TEST_PASS
}

SETUP( TestOrderL ) {

  _count++;

  ASSERT( (_count == 13) || (_count == 8) );

  TEST_PASS
}

CLEANUP( TestOrderA ) {

  _count++;
  
  ASSERT( _count == 4 );

  TEST_PASS
}

SETUP( UnmatchedB ) {
  /* Should never run */
  _count++;

  ASSERT( _count == 6 );

  TEST_PASS
}

TEST_WITH_ID( TestOrderP, 1001 ) {

  _count++;
  
  ASSERT( (_count == 27) || (_count == 30) );

  TEST_PASS
}

CLEANUP( UnmatchedC ) {
  /* Should never run */
  _count++;
  
  ASSERT( _count == 4 );

  TEST_PASS
}

CLEANUP( TestOrderZ ) {

  _count++;
  
  ASSERT( _count == 20 );

  TEST_PASS
}

TEST_WITH_ID( TestOrderZ, 50 ) {

  _count++;
  
  ASSERT( _count == 19 );

  TEST_PASS
}

SETUP( TestOrderZ ) {

  _count++;
  
  ASSERT( _count == 18 );

  TEST_PASS
}

TEST_WITH_ID( TestOrderG, 2 ) {

  _count++;
  
  ASSERT( (_count == 11) || (_count == 14) );

  TEST_PASS
}

TEST_WITH_ID( SetupFail, 40 ) {

  _count++;
  
  ASSERT( _count == 16 );

  TEST_PASS
}

SETUP( SetupFail ) {

  _count++;
  
  ASSERT( _count == 16 );

  ASSERT( 1 == 2 );
  
  TEST_PASS
}

CLEANUP( SetupFail ) {

  _count++;
  
  ASSERT( _count == 17 );

  TEST_PASS
}

TEST_WITH_ID( CleaupFail, 70 ) {

  _count++;
  
  ASSERT( _count == 22 );

  TEST_PASS
}

SETUP( CleaupFail ) {

  _count++;
  
  ASSERT( _count == 21 );

  TEST_PASS
}

CLEANUP( CleaupFail ) {

  _count++;
  
  ASSERT( _count == 23 );

  ASSERT( 1 == 2 );
  
  TEST_PASS
}

TEST_WITH_ID( TestOrderV, 1000 ) {

  _count++;
  
  ASSERT( (_count == 30) || (_count == 27) );

  TEST_PASS
}

SETUP( TestOrderP ) {

  _count++;
  
  ASSERT( (_count == 26) || (_count == 29) );

  TEST_PASS
}

CLEANUP( TestOrderB ) {

  _count++;
  
  ASSERT( _count == 7 );

  TEST_PASS
}

CLEANUP( TestOrderP ) {

  _count++;
  
  ASSERT( (_count == 28) || (_count == 31) );

  TEST_PASS
}

CLEANUP( TestOrderG ) {

  _count++;
  
  ASSERT( (_count == 12) || (_count == 15) );

  TEST_PASS
}

TEST_WITH_ID( TestOrderL, 0 ) {

  _count++;
  
  ASSERT( (_count == 14) || (_count == 9) );

  TEST_PASS
}

SETUP( TestOrderB ) {

  _count++;
  
  ASSERT( _count == 5 );

  TEST_PASS
}

SETUP( TestOrderA ) {

  _count++;
  
  ASSERT( _count == 2 );

  TEST_PASS
}

END_SUITE

