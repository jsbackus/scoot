/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Jeff Backus <jeff@jsbackus.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*! \file
 * Unit tests related to testing failing suite setup function.
 */

#include <scoot.h>

/* Generally bad juju to encourage interaction between tests, but it is the
   easiest way to verify order. */
int _setupFailCount = 0;

START_SUITE( SuiteSetupFail )

SUITE_CLEANUP {
  _setupFailCount++;

  ASSERT( _setupFailCount == 2 );
  
  TEST_PASS
}

TEST( TestOrderB ) {

  _setupFailCount++;

  ASSERT( _setupFailCount == 2 );

  TEST_PASS
}

SUITE_SETUP {
  _setupFailCount++;

  ASSERT( 2 == 1 );
  
  TEST_PASS
}

END_SUITE

