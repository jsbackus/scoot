/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Jeff Backus <jeff@jsbackus.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*! \file
 * Contains the public definitions for SCOOT.
 */

#ifndef __SCOOT_H__
#define __SCOOT_H__

#ifdef __cplusplus
#include <cstdio>
#include <cstring>
#include <exception>
#include <cstdint>
#else
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#endif


#define SCOOT_VERSION 0
#define SCOOT_COPYRIGHT_YEAR 2017
#define SCOOT_URL "https://gitlab.com/jsbackus/scoot"

#ifdef __unix__
#define ENDL "\n"
#else
#define ENDL "\r\n"
#endif

#ifdef __unix__

#ifdef __cplusplus
#define SCOOT_FN extern "C"
#else
#define SCOOT_FN extern
#endif

#else

#ifdef SCOOT_INTERNAL

#ifdef __cplusplus
#define SCOOT_FN __declspec(dllexport) extern "C"
#else
#define SCOOT_FN __declspec(dllexport) extern
#endif

#else

#ifdef __cplusplus
#define SCOOT_FN extern "C"
#else
#define SCOOT_FN extern
#endif

#endif

#endif

#define LARGEST_INT_TYPE int64_t
#define LARGEST_UINT_TYPE uint64_t
#define LARGEST_FLOAT_TYPE double


typedef enum {LOG_PASS=1, LOG_FAIL, LOG_TEST, LOG_SUITE, LOG_ERROR, LOG_WARN,
	      LOG_INFO, LOG_DEBUG, LOG_SUITE_RESULTS, LOG_FINAL_RESULTS,
	      LOG_CRITICAL} scoot_log_msg_t;

typedef void (*scoot_log_msg_fn_t)( scoot_log_msg_t msgType, const char* fmt, ... );
typedef void (*scoot_report_test_fail_fn_t)( const char* filename, int line, const char* fmt, ... );
typedef char* (*scoot_stringify_compare_fn_t)( LARGEST_INT_TYPE int_lhs,
					       LARGEST_UINT_TYPE uint_lhs,
					       LARGEST_FLOAT_TYPE flt_lhs,
					       const char* opStr,
					       LARGEST_INT_TYPE int_rhs,
					       LARGEST_UINT_TYPE uint_rhs,
					       LARGEST_FLOAT_TYPE flt_rhs );
typedef void (*scoot_register_callbacks_fn_t)( scoot_log_msg_fn_t logFN,
					       scoot_report_test_fail_fn_t failFN,
					       scoot_stringify_compare_fn_t strFN );

/*!
 * \brief Formats the specified message and routes it to the appropriate 
 * output(s).
 *
 * All \r and \n characters are stripped and replaced with a suitable value.
 * \addtogroup log_fns
 */
#ifdef SCOOT_INTERNAL
SCOOT_FN void scoot_log_message( scoot_log_msg_t msgType, const char* fmt, ... );
#else
#define scoot_log_message( ... ) _scoot_log_message(__VA_ARGS__)
#endif

/*!
 * \brief Reports a failure for the current test.
 *
 * Not intended for direct use by clients.
 * \addtogroup log_fns
 */
#ifdef SCOOT_INTERNAL
SCOOT_FN void scoot_report_test_fail( const char* filename, int line, const char* fmt, ... );
#else
#define scoot_report_test_fail( ... ) _scoot_report_test_fail(__VA_ARGS__)
#endif

/*!
 * \brief Returns a string representation of the specified comparison.
 *
 * Not intended for direct use by clients.
 * \addtogroup log_fns
 */
#ifdef SCOOT_INTERNAL
SCOOT_FN char* scoot_stringify_compare( LARGEST_INT_TYPE int_lhs,
					LARGEST_UINT_TYPE uint_lhs,
					LARGEST_FLOAT_TYPE flt_lhs,
					const char* opStr,
					LARGEST_INT_TYPE int_rhs,
					LARGEST_UINT_TYPE uint_rhs,
					LARGEST_FLOAT_TYPE flt_rhs );
#else
#define scoot_stringify_compare( X, Y, Z ) \
  _scoot_stringify_compare( (LARGEST_INT_TYPE) X,  \
			    (LARGEST_UINT_TYPE) X,  \
			    (LARGEST_FLOAT_TYPE) X, \
			    #Y, \
			    (LARGEST_INT_TYPE) Z, \
			    (LARGEST_UINT_TYPE) Z, \
			    (LARGEST_FLOAT_TYPE) Z )
#endif

#define SCOOT_CONCAT4( W, X, Y, Z) W##X##Y##Z
#define SCOOT_CONCAT3( X, Y, Z) X##Y##Z
#define SCOOT_CONCAT( X, Y ) X##Y
#define SCOOT_AS_STR( X ) #X

#define TEST_PASS return 0;
#define TEST_FAIL return 1;

#define ASSERT( STATEMENT ) if( !(STATEMENT) ) { scoot_report_test_fail( __FILE__, __LINE__, "Assertion failed: %s", #STATEMENT ); TEST_FAIL }
#define ASSERT_WITH_MSG( STATEMENT, MSG ) if( !(STATEMENT) ) { scoot_report_test_fail( __FILE__, __LINE__, "Assertion failed: %s", MSG ); TEST_FAIL }
#define ASSERT_EQUALS( LHS, RHS ) ASSERT_WITH_MSG( (LHS == RHS), scoot_stringify_compare( LHS, ==, RHS))
#define ASSERT_NOT_EQUALS( LHS, RHS ) ASSERT_WITH_MSG( (LHS != RHS), scoot_stringify_compare( LHS, !=, RHS))
#define ASSERT_LESS_THAN( LHS, RHS ) ASSERT_WITH_MSG( (LHS < RHS), scoot_stringify_compare( LHS, <, RHS))
#define ASSERT_LESS_THAN_EQUALS( LHS, RHS ) ASSERT_WITH_MSG( (LHS <= RHS), scoot_stringify_compare( LHS, <=, RHS))
#define ASSERT_GREATER_THAN( LHS, RHS ) ASSERT_WITH_MSG( (LHS > RHS), scoot_stringify_compare( LHS, >, RHS))
#define ASSERT_GREATER_THAN_EQUALS( LHS, RHS ) ASSERT_WITH_MSG( (LHS >= RHS), scoot_stringify_compare( LHS, >=, RHS))

#define START_SUITE( SUITE_NAME ) SCOOT_FN void SCOOT_CONCAT(_SCOOT_TEST_SUITE_,SUITE_NAME)(char* buf, int size) { if( buf != NULL ) {strncpy(buf, #SUITE_NAME, size); }}; scoot_log_msg_fn_t _scoot_log_message; scoot_report_test_fail_fn_t _scoot_report_test_fail; scoot_stringify_compare_fn_t _scoot_stringify_compare; SCOOT_FN void _SCOOT_REGISTER_CALLBACKS( scoot_log_msg_fn_t logFN, scoot_report_test_fail_fn_t failFN, scoot_stringify_compare_fn_t strFN) { _scoot_log_message = logFN; _scoot_report_test_fail = failFN; _scoot_stringify_compare = strFN; }

#define END_SUITE

#define ADD_SUITE_WITH_ID( SUITE_NAME, ID ) SCOOT_FN void SCOOT_CONCAT4(_SCOOT_RUN_SUITE_,ID,_,SUITE_NAME)(char* buf, int size) { if( buf != NULL ) {strncpy(buf, #SUITE_NAME, size); }}

#define ADD_SUITE( SUITE_NAME ) ADD_SUITE_WITH_ID(SUITE_NAME,1)

#ifdef __cplusplus
#define TEST_WITH_ID( TEST_NAME, ID ) int TEST_NAME(); SCOOT_FN int SCOOT_CONCAT4(_SCOOT_TEST_FN_,ID,_,TEST_NAME)() { try { return TEST_NAME(); } catch (exception& e) { scoot_report_test_fail( __FILE__, __LINE__, "Unhandled exception caught in %s: %s", #TEST_NAME, e.what()); TEST_FAIL} catch (...) { scoot_report_test_fail( __FILE__, __LINE__, "Unhandled exception caught in %s", #TEST_NAME); TEST_FAIL}} int TEST_NAME()
#else
#define TEST_WITH_ID( TEST_NAME, ID ) SCOOT_FN int SCOOT_CONCAT4(_SCOOT_TEST_FN_,ID,_,TEST_NAME)()
#endif
#define TEST( TEST_NAME ) TEST_WITH_ID(TEST_NAME,1)
#define SETUP( TEST_NAME ) SCOOT_FN int SCOOT_CONCAT(_SCOOT_SETUP_FN_,TEST_NAME)()
#define CLEANUP( TEST_NAME ) SCOOT_FN int SCOOT_CONCAT(_SCOOT_CLEANUP_FN_,TEST_NAME)()

#define SUITE_SETUP SCOOT_FN int _SCOOT_SUITE_SETUP_FN()
#define SUITE_CLEANUP SCOOT_FN int _SCOOT_SUITE_CLEANUP_FN()

#define DEFAULT_SETUP SCOOT_FN int _SCOOT_DEFAULT_SETUP_FN()
#define DEFAULT_CLEANUP SCOOT_FN int _SCOOT_DEFAULT_CLEANUP_FN()

#endif
