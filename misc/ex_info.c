#define _GNU_SOURCE
#include <dlfcn.h>
#include <link.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int (*scoot_test_fn_t)( );
struct _test_fn_t {
  scoot_test_fn_t f;
  struct test_fn_t* prev;
  struct test_fn_t* next;
};
typedef struct _test_fn_t test_fn_t;

int
main(int argc, char *argv[])
{
  void *handle;
  struct link_map* lm;
  int j;

  if (argc != 2) {
    fprintf(stderr, "Usage: %s <libpath>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Obtain a handle for shared objects specified on command line */

  handle = dlopen(argv[1], RTLD_NOW);
  if (handle == NULL) {
    fprintf(stderr, "dlopen() failed: %s\n", dlerror());
    exit(EXIT_FAILURE);
  }

  /* Get the link map */
  if( dlinfo( handle, RTLD_DI_LINKMAP, &lm ) == -1 ) {
    fprintf(stderr, "RTLD_DI_LINKMAP failed: %s\n", dlerror());
    exit(EXIT_FAILURE);
  }

  ElfW(Dyn)* pDyn = NULL;
  char buff[256];
  
  while( lm != NULL ) {
    printf("%s:\n", lm->l_name);
    ElfW(Addr) addr = lm->l_addr;
    pDyn = lm->l_ld;
    lm = lm->l_next;

    int i = 0;
    int done = 0;
    ElfW(Addr) *symTable = NULL;
    ElfW(Addr) symItemSize = 0;
    ElfW(Addr) *strTable = NULL;
    ElfW(Addr) strTableSize = 0;
    ElfW(Addr) *sysvHash = NULL;
    ElfW(Addr) *gnuHash = NULL;
    unsigned int numSymbols = 0;
    
    while( !done ) {
      switch( pDyn->d_tag ) {
      case( DT_NULL ):
	snprintf(&buff[0], 255, "DT_NULL");
	done = 1;
	break;
      case( DT_NEEDED ):
	snprintf(&buff[0], 255, "DT_NEEDED");
	break;
      case( DT_PLTRELSZ ):
	snprintf(&buff[0], 255, "DT_PLTRELSZ");
	break;
      case( DT_PLTGOT ):
	snprintf(&buff[0], 255, "DT_PLTGOT");
	break;
      case( DT_HASH ):
	snprintf(&buff[0], 255, "DT_HASH");
	sysvHash = pDyn->d_un.d_ptr;
	break;
      case( DT_STRTAB ):
	snprintf(&buff[0], 255, "DT_STRTAB");
	strTable = pDyn->d_un.d_ptr; //addr + pDyn->d_un.d_ptr;
	break;
      case( DT_SYMTAB ):
	snprintf(&buff[0], 255, "DT_SYMTAB");
	symTable = pDyn->d_un.d_ptr;
	break;
      case( DT_RELA ):
	snprintf(&buff[0], 255, "DT_RELA");
	break;
      case( DT_RELASZ ):
	snprintf(&buff[0], 255, "DT_RELASZ");
	break;
      case( DT_RELAENT ):
	snprintf(&buff[0], 255, "DT_RELAENT");
	break;
      case( DT_STRSZ ):
	snprintf(&buff[0], 255, "DT_STRSZ");
	strTableSize = pDyn->d_un.d_val;
	break;
      case( DT_SYMENT ):
	snprintf(&buff[0], 255, "DT_SYMENT");
	symItemSize = pDyn->d_un.d_val;
	break;
      case( DT_INIT ):
	snprintf(&buff[0], 255, "DT_INIT");
	break;
      case( DT_FINI ):
	snprintf(&buff[0], 255, "DT_FINI");
	break;
      case( DT_SONAME ):
	snprintf(&buff[0], 255, "DT_SONAME");
	break;
      case( DT_RPATH ):
	snprintf(&buff[0], 255, "DT_RPATH");
	break;
      case( DT_SYMBOLIC ):
	snprintf(&buff[0], 255, "DT_SYMBOLIC");
	break;
      case( DT_REL ):
	snprintf(&buff[0], 255, "DT_REL");
	break;
      case( DT_RELSZ ):
	snprintf(&buff[0], 255, "DT_RELSZ");
	break;
      case( DT_RELENT ):
	snprintf(&buff[0], 255, "DT_RELENT");
	break;
      case( DT_PLTREL ):
	snprintf(&buff[0], 255, "DT_PLTREL");
	break;
      case( DT_DEBUG ):
	snprintf(&buff[0], 255, "DT_DEBUG");
	break;
      case( DT_TEXTREL ):
	snprintf(&buff[0], 255, "DT_TEXTREL");
	break;
      case( DT_JMPREL ):
	snprintf(&buff[0], 255, "DT_JMPREL");
	break;
      case( DT_BIND_NOW ):
	snprintf(&buff[0], 255, "DT_BIND_NOW");
	break;
      case( DT_INIT_ARRAY ):
	snprintf(&buff[0], 255, "DT_INIT_ARRAY");
	break;
      case( DT_FINI_ARRAY ):
	snprintf(&buff[0], 255, "DT_FINI_ARRAY");
	break;
      case( DT_INIT_ARRAYSZ ):
	snprintf(&buff[0], 255, "DT_INIT_ARRAYSZ");
	break;
      case( DT_FINI_ARRAYSZ ):
	snprintf(&buff[0], 255, "DT_FINI_ARRAYSZ");
	break;
      case( DT_RUNPATH ):
	snprintf(&buff[0], 255, "DT_RUNPATH");
	break;
      case( DT_FLAGS ):
	snprintf(&buff[0], 255, "DT_FLAGS");
	break;
      case( DT_ENCODING ):
	snprintf(&buff[0], 255, "DT_ENCODING");
	break;
      case( DT_PREINIT_ARRAYSZ ):
	snprintf(&buff[0], 255, "DT_PREINIT_ARRAYSZ");
	break;
      case( DT_NUM ):
	snprintf(&buff[0], 255, "DT_NUM");
	break;
      case( DT_LOOS ):
	snprintf(&buff[0], 255, "DT_LOOS");
	break;
      case( DT_HIOS ):
	snprintf(&buff[0], 255, "DT_HIOS");
	break;
      case( DT_LOPROC ):
	snprintf(&buff[0], 255, "DT_LOPROC");
	break;
      case( DT_HIPROC ):
	snprintf(&buff[0], 255, "DT_HIPROC");
	break;
      case( DT_PROCNUM ):
	snprintf(&buff[0], 255, "DT_PROCNUM");
	break;
      case( DT_VALRNGLO ):
	snprintf(&buff[0], 255, "DT_VALRNGLO");
	break;
      case( DT_GNU_PRELINKED ):
	snprintf(&buff[0], 255, "DT_GNU_PRELINKED");
	break;
      case( DT_GNU_CONFLICTSZ ):
	snprintf(&buff[0], 255, "DT_GNU_CONFLICTSZ");
	break;
      case( DT_GNU_LIBLISTSZ ):
	snprintf(&buff[0], 255, "DT_GNU_LIBLISTSZ");
	break;
      case( DT_CHECKSUM ):
	snprintf(&buff[0], 255, "DT_CHECKSUM");
	break;
      case( DT_PLTPADSZ ):
	snprintf(&buff[0], 255, "DT_PLTPADSZ");
	break;
      case( DT_MOVEENT ):
	snprintf(&buff[0], 255, "DT_MOVEENT");
	break;
      case( DT_MOVESZ ):
	snprintf(&buff[0], 255, "DT_MOVESZ");
	break;
      case( DT_FEATURE_1 ):
	snprintf(&buff[0], 255, "DT_FEATURE_1");
	break;
      case( DT_POSFLAG_1 ):
	snprintf(&buff[0], 255, "DT_POSFLAG_1");
	break;
      case( DT_SYMINSZ ):
	snprintf(&buff[0], 255, "DT_SYMINSZ");
	printf("syminsz: %0d\n", pDyn->d_un.d_val);
	break;
      case( DT_SYMINENT ):
	snprintf(&buff[0], 255, "DT_SYMINENT");
	printf("syminent: %0d\n", pDyn->d_un.d_val);
	break;

      case( DT_ADDRRNGLO ):
	snprintf(&buff[0], 255, "DT_ADDRRNGLO");
	break;
      case( DT_GNU_HASH ):
	snprintf(&buff[0], 255, "DT_GNU_HASH");
	gnuHash = pDyn->d_un.d_ptr;
	break;
      case( DT_TLSDESC_PLT ):
	snprintf(&buff[0], 255, "DT_TLSDESC_PLT");
	break;
      case( DT_TLSDESC_GOT ):
	snprintf(&buff[0], 255, "DT_TLSDESC_GOT");
	break;
      case( DT_GNU_CONFLICT ):
	snprintf(&buff[0], 255, "DT_GNU_CONFLICT");
	break;
      case( DT_GNU_LIBLIST ):
	snprintf(&buff[0], 255, "DT_GNU_LIBLIST");
	break;
      case( DT_CONFIG ):
	snprintf(&buff[0], 255, "DT_CONFIG");
	break;
      case( DT_DEPAUDIT ):
	snprintf(&buff[0], 255, "DT_DEPAUDIT");
	break;
      case( DT_AUDIT ):
	snprintf(&buff[0], 255, "DT_AUDIT");
	break;
      case( DT_PLTPAD ):
	snprintf(&buff[0], 255, "DT_PLTPAD");
	break;
      case( DT_MOVETAB ):
	snprintf(&buff[0], 255, "DT_MOVETAB");
	break;
      case( DT_SYMINFO ):
	snprintf(&buff[0], 255, "DT_SYMINFO");
	printf("syminfo: %10p\n", pDyn->d_un.d_ptr);
	break;


      default:
	snprintf(&buff[0], 255, "UNKNOWN: 0x%0x", pDyn->d_tag );
      }
      printf("\t%2d: %s\n", i, &buff[0]);
      i++;
      pDyn++;
    }
    
    printf("\n\tString Table: %0d bytes, %10p\n", strTableSize, strTable);
    test_fn_t* f_list = NULL;
    test_fn_t* new_node = NULL;
    if( strTable != NULL && 0 < strTableSize ) {
      i = 0;
      char* pStr = strTable;
      int matched;
      while(pStr < (char*)strTable + strTableSize) {
	matched = 0;
	if( *pStr == NULL ) {
	  strncpy(&buff[0], "NULL", 255);
	  pStr++;
	} else {
	  if( 0 == strncmp( pStr, "SCOOT_TEST_FN_", strlen("SCOOT_TEST_FN_")) ) {
	    matched = 1;
	    new_node = (test_fn_t*)malloc(sizeof(test_fn_t));
	    new_node->next = NULL;
	    new_node->f = (scoot_test_fn_t)(dlsym(handle, pStr));
	    new_node->prev = f_list;

	    if( f_list != NULL ) {
	      f_list->next = new_node;
	    }
	    f_list = new_node;
	    if( dlerror() != NULL ) {
	      printf("Error: %s\n", dlerror());
	    }
	  }
	  strncpy(&buff[0], pStr, 255);
	  pStr+= strlen(pStr);
	}
	printf("\t\t%2d: '%s'", i, &buff[0]);
	if( matched ) {
	  printf(" <Test Function>");
	}
	printf("\n");
	i++;
      }

      printf("\nTests:\n");
      new_node = f_list;
      i = 0;
      while(new_node != NULL && new_node->prev != NULL) {
	new_node = new_node->prev;
      }
      while(new_node != NULL) {
	printf("\t\t%2d: Results: %d\n", i, new_node->f());
	new_node = new_node->next;
      }
    }
  }
  
  exit(EXIT_SUCCESS);
}
