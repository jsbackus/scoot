/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Jeff Backus <jeff@jsbackus.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*! \file
 * Contains the POSIX-specific implementations of certain functions defined in 
 * scoot.h
 */
#ifdef __unix__
/* Required for dynamic libraries */
#define _GNU_SOURCE
#include <dlfcn.h>
#include <link.h>
/* End required for dynamic libraries */

#include <dirent.h>
#include <stdio.h>
#include <string.h>

#include "scoot_internal.h"

SCOOT_FN scoot_suite_handle_t* loadTestSuite( const char* fn ) {
  scoot_suite_handle_t* retVal = malloc(sizeof(scoot_suite_handle_t));
  CHECK_NEW( retVal );

  /* Attempt to load with dlopen() */
  retVal->hFile = dlopen(fn, RTLD_NOW);
  if( retVal->hFile  == NULL ) {
    scoot_log_message( LOG_ERROR, "Failed to open Test Suite file %s: %s",
		       fn, dlerror());
    free(retVal);
    retVal = NULL;
    return NULL;
  }

  return retVal;
}

SCOOT_FN scoot_suite_token_t tokenizeTestSuiteFile( scoot_suite_handle_t* suite ) {
  if( suite == NULL || suite->hFile == NULL ) {
    scoot_log_message( LOG_DEBUG,
		       "TokenizeTestSuiteFile called with invalid suite handle." );
    return NULL;
  }
  
  /* Get the link map */
  struct link_map* lm;
  
  if( dlinfo( suite->hFile, RTLD_DI_LINKMAP, &lm ) == -1 ) {
    scoot_log_message( LOG_ERROR, "RTLD_DI_LINKMAP failed: %s", dlerror());
    return NULL;
  }

  if( lm == NULL ) {
    scoot_log_message( LOG_ERROR, "Empty link map!");
    return NULL;
  }
  ElfW(Dyn)* pDyn = lm->l_ld;
  ElfW(Addr) addr = lm->l_addr;

  int done = 0;
  ElfW(Addr) *strTable = NULL;
  ElfW(Addr) strTableSize = 0;

  /* Find the string table location and size */
  while( !done ) {
    switch( pDyn->d_tag ) {
    case( DT_NULL ):
      /* End of dynamic table */
      done = 1;
      break;
    case( DT_STRTAB ):
      /* String table */
      strTable = (ElfW(Addr)*)pDyn->d_un.d_ptr;
      break;
    case( DT_STRSZ ):
      /* Size (in bytes) of string table */
      strTableSize = pDyn->d_un.d_val;
      break;
    }
    pDyn++;
  }

  /* Copy into suite handle. */
  if( strTable != NULL && 0 < strTableSize ) {
    suite->pStrTable = (char*)strTable;
    suite->strTableSize = (unsigned int)strTableSize;
  }

  /* Return first token */
  return (char*)strTable;
}

SCOOT_FN int extractTestSuiteToken( scoot_suite_handle_t suite,
			   scoot_suite_token_t* token,
			   char* buf, int buf_size ) {
  if( suite.hFile == NULL || suite.pStrTable == NULL ||
      suite.strTableSize == 0 || token == NULL || *token == NULL ||
      buf == NULL || buf_size <= 0 ) {
    return -1;
  }
  /* Calculate the end of the table once, so that we don't have to keep 
     typing it. */
  char* tableEnd = (suite.pStrTable + suite.strTableSize);

  /* Advance to the next non-empty string in the table. */
  while( **token == 0 && *token < tableEnd ) {
    *token = ((char*)(*token)) + 1;
  }

  /* Make sure we haven't advanced beyond the end of the table. */
  if( tableEnd <= *token ) {
    *token = NULL;
    return 0;
  }

  /* Copy the string into the provided buffer */
  strncpy( buf, *token, buf_size );

  /* Update token */
  *token = ((char*)(*token) + strlen(*token));

  return 0;
}

SCOOT_FN int closeTestSuite( scoot_suite_handle_t suite ) {
  if( suite.hFile == NULL ) {
    return 0;
  }

  if( dlclose( suite.hFile ) == 0 ) {
    return 0;
  }

  scoot_log_message( LOG_ERROR, "Error closing test suite: %s", dlerror());
  return -1;
}

SCOOT_FN scoot_test_fn_t extractTestSuiteFunction( scoot_suite_handle_t suite,
						   char* fnName ) {
  if( suite.hFile == NULL || fnName == NULL || *fnName == 0 ) {
    return NULL;
  }

  /* Attempt to retrieve function pointer */
  scoot_test_fn_t fn = (scoot_test_fn_t)(dlsym(suite.hFile, fnName));

  if( fn == NULL ) {
    scoot_log_message( LOG_ERROR, "Error retrieving function %s: %s", fnName,
		       dlerror() );
  }

  return fn;
}

SCOOT_FN void scanPath( const char* path, int bRecurse ) {

  if( path == NULL ) {
    return;
  }

  scoot_log_message( LOG_DEBUG, "Scanning path '%s' ...", path );

  char buf[256];
  int len = strlen( path );
  
  /* Check all .scoot files in the current directory */
  DIR* pD;
  struct dirent* pDir;
  pD = opendir( path );
  if( pD == NULL ) {
    scoot_log_message( LOG_ERROR, "Unable to open path '%s'!", path );
    return;
  }
  
  while( (pDir = readdir(pD)) != NULL ) {
    /* Make sure we aren't looking at . or .. */
    if( strcmp(".", pDir->d_name) != 0 &&
	strcmp("..", pDir->d_name) != 0 ) {
      
      /* See if entry is a directory */
      if( pDir->d_type == DT_DIR ) {
	/* If recursion is requested, process this directory */
	if( bRecurse != 0 ) {
	  strncpy( &buf[0], path, 254 );
	  buf[len] = '/';
	  buf[len + 1] = 0;
	  strncat( &buf[0], pDir->d_name, 255 );
	  scanPath( &buf[0], bRecurse );
	}
      } else {
	/* Assume we're dealing with a file. */
	
	/* Find last dot. */
	char* pExt = NULL;
	char* pTmp = pDir->d_name;
	while( *pTmp != 0 ) {
	  if( *pTmp == '.' ) {
	    pExt = pTmp;
	  }
	  pTmp++;
	}
	if( pExt != NULL ) {
	  pExt++;
	}
	/* If we have an extension. See if it is a shared library. */
	if( pExt != NULL ) {
	  if( strcmp( pExt, SCOOT_EXT ) == 0 ) {
	    /* If so, prepend the current directory and process. */
	    strncpy( &buf[0], path, 254 );
	    buf[len] = '/';
	    buf[len + 1] = 0;
	    strncat( &buf[0], pDir->d_name, 255 );
	  
	    /* Scan file for SCOOT items. */
	    scanFile(&buf[0]);
	  }
	}
      }
    }
  }
  closedir( pD );
}
#endif
