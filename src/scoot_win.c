/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Jeff Backus <jeff@jsbackus.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*! \file
 * Contains the Microsoft Windows-specific implementations of certain functions
 * defined in scoot.h
 *
 * Lots of DLL-based code was based on example code from:
 * - https://msdn.microsoft.com/en-us/library/ms809762.aspx
 */
#ifdef _WIN32
/* Required for dynamic libraries */
/* End required for dynamic libraries */

#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include <stdio.h>
#include <string.h>

#include "scoot_internal.h"

SCOOT_FN scoot_suite_handle_t* loadTestSuite( const char* fn ) {
  scoot_suite_handle_t* retVal = malloc(sizeof(scoot_suite_handle_t));
  CHECK_NEW( retVal );

  /* Attempt to load the DLL with LoadLibrary() */
  retVal->hFile = LoadLibrary( fn );
  if( retVal->hFile == NULL ) {
    scoot_log_message( LOG_ERROR, "Failed to open Test Suite file %s: %d",
		       fn, GetLastError());
    free(retVal);
    retVal = NULL;
    return NULL;
  }
  
  return retVal;
}

SCOOT_FN scoot_suite_token_t tokenizeTestSuiteFile( scoot_suite_handle_t* suite ) {

  if( suite == NULL || suite->hFile == NULL ) {
    scoot_log_message( LOG_DEBUG,
		       "TokenizeTestSuiteFile called with invalid suite handle." );
    return NULL;
  }
  
  LPVOID lpMapView = (LPVOID)suite->hFile;
  PIMAGE_DOS_HEADER pDosHdr = (PIMAGE_DOS_HEADER)lpMapView;
  PIMAGE_NT_HEADERS pNtHdr = (PIMAGE_NT_HEADERS)((char*)pDosHdr + pDosHdr->e_lfanew);
  PIMAGE_FILE_HEADER pNtFileHdr = &pNtHdr->FileHeader;

  PIMAGE_OPTIONAL_HEADER pNtOptHdr = &pNtHdr->OptionalHeader;

  char* pExportsVirtualAddress = NULL;
  PIMAGE_SECTION_HEADER pSectionHdr = (PIMAGE_SECTION_HEADER)((char*)pNtOptHdr + pNtFileHdr->SizeOfOptionalHeader);
  PIMAGE_EXPORT_DIRECTORY pExportDir = NULL;
  int i;
  for( i = 0; i < pNtFileHdr->NumberOfSections; i++ ) {
    if( strncmp( pSectionHdr, ".edata", 8 ) == 0 ) {
      pExportsVirtualAddress = (char*)pSectionHdr->VirtualAddress;
      pExportDir = (PIMAGE_EXPORT_DIRECTORY)((int)lpMapView + (int)pExportsVirtualAddress);
    }

    pSectionHdr++;
  }
  
  suite->pOffset = ((char*)pExportDir - pExportsVirtualAddress);
  suite->pStrTable = pExportDir->AddressOfNames + suite->pOffset;
  suite->strTableSize = pExportDir->NumberOfNames;

  return (scoot_suite_token_t)suite->pStrTable;
}

SCOOT_FN int extractTestSuiteToken( scoot_suite_handle_t suite,
			   scoot_suite_token_t* token,
			   char* buf, int buf_size ) {
  if( suite.hFile == NULL || suite.pStrTable == NULL ||
      suite.strTableSize == 0 || token == NULL || *token == NULL ||
      buf == NULL || buf_size <= 0 ) {
    return -1;
  }

  /* Calculate the end of the table once, so that we don't have to keep 
     typing it. */
  int* tableEnd = ((int*)suite.pStrTable + suite.strTableSize);

  /* Copy the string into the provided buffer */
  strncpy( buf, (char*)(**token + suite.pOffset), buf_size );

  /* Advance to the next string in the table. */
  *token = ((int*)(*token)) + 1;

  /* Make sure we haven't advanced beyond the end of the table. */
  if( tableEnd <= *token ) {
    *token = NULL;
  }

  return 0;
}

SCOOT_FN int closeTestSuite( scoot_suite_handle_t suite ) {
  if( suite.hFile == NULL ) {
    return 0;
  }
  if( FreeLibrary( suite.hFile ) != 0 ) {
    return 0;
  }

  scoot_log_message( LOG_ERROR, "Error closing test suite: %d", GetLastError());
  return -1;
}

SCOOT_FN scoot_test_fn_t extractTestSuiteFunction( scoot_suite_handle_t suite,
						   char* fnName ) {
  if( suite.hFile == NULL || fnName == NULL || *fnName == 0 ) {
    return NULL;
  }

  /* Attempt to retrieve function pointer */
  scoot_test_fn_t fn = (scoot_test_fn_t)(GetProcAddress(suite.hFile, fnName));
  
  if( fn == NULL ) {
    scoot_log_message( LOG_ERROR, "Error retrieving function %s: %d", fnName,
		       GetLastError() );
  }

  return fn;
}

SCOOT_FN void scanPath( const char* path, int bRecurse ) {

  if( path == NULL ) {
    return;
  }

  scoot_log_message( LOG_DEBUG, "Scanning path '%s' ...", path );

  char buf[256];
  int len = strlen( path );
  WIN32_FIND_DATA ffd;
  HANDLE hDir = INVALID_HANDLE_VALUE;
  TCHAR szDir[MAX_PATH];

  /* Copy specified path to a datastructure that the WinAPI wants to see */
  StringCchCopy( szDir, MAX_PATH, path);
  /* Append a wildcard so that we pick up everything. We'll filter, ourselves */
  StringCchCat( szDir, MAX_PATH, TEXT("\\*") );

  hDir = FindFirstFile( szDir, &ffd );
  if( hDir == INVALID_HANDLE_VALUE ) {
    scoot_log_message( LOG_ERROR, "Unable to open path '%s'!", path );
    return;
  }

  do {
    /* Make sure we aren't looking at . or .. */
    if( strcmp(".", ffd.cFileName) != 0 &&
	strcmp("..", ffd.cFileName) != 0 ) {
      
      /* See if entry is a directory */
      if( ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) {
	/* If recursion is requested, process this directory */
	if( bRecurse != 0 ) {
	  strncpy( &buf[0], path, 254 );
	  buf[len] = '/';
	  buf[len + 1] = 0;
	  strncat( &buf[0], ffd.cFileName, 255 );
	  scanPath( &buf[0], bRecurse );
	}
      } else {
	/* Assume we're dealing with a file. */
	
	/* Find last dot. */
	char* pExt = NULL;
	char* pTmp = ffd.cFileName;
	while( *pTmp != 0 ) {
	  if( *pTmp == '.' ) {
	    pExt = pTmp;
	  }
	  pTmp++;
	}
	if( pExt != NULL ) {
	  pExt++;
	}
	/* If we have an extension. See if it is a shared library. */
	if( pExt != NULL ) {
	  if( strcmp( pExt, SCOOT_EXT ) == 0 ) {
	    /* If so, prepend the current directory and process. */
	    strncpy( &buf[0], path, 254 );
	    buf[len] = '/';
	    buf[len + 1] = 0;
	    strncat( &buf[0], ffd.cFileName, 255 );
	  
	    /* Scan file for SCOOT items. */
	    scanFile(&buf[0]);
	  }
	}
      }
    }
  } while( FindNextFile( hDir, &ffd ) != 0 );
  
  FindClose( hDir );
}
#endif
