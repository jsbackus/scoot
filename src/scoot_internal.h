/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Jeff Backus <jeff@jsbackus.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*! \file
 * Contains definitions for types and data structures internal to SCOOT.
 */

#ifndef __SCOOT_INTERNAL_H__
#define __SCOOT_INTERNAL_H__

#include <stdlib.h>

#define SCOOT_INTERNAL
#include <scoot.h>

#ifndef SCOOT_EXT
#define SCOOT_EXT "scoot"
#endif

typedef struct _scoot_suite_handle_t {
  void* hFile;
  char* pStrTable;
  unsigned int strTableSize;
  #ifdef _WIN32
  char* pOffset;
  #endif
} scoot_suite_handle_t;

#ifdef _WIN32
typedef int* scoot_suite_token_t;
#else
typedef char* scoot_suite_token_t;
#endif

#define CHECK_NEW( X ) if( X == NULL ) { fprintf(stderr, "out of memory\n"); exit(1); }

typedef int (*scoot_test_fn_t)( );
typedef struct _scoot_test_node_t {
  char* name;
  int id;
  scoot_test_fn_t setup;
  scoot_test_fn_t test;
  scoot_test_fn_t cleanup;
  struct _scoot_test_node_t* prev;
  struct _scoot_test_node_t* next;
} scoot_test_node_t;

typedef struct _scoot_subsuite_node_t {
  char* name;
  int id;
  struct _scoot_subsuite_node_t* prev;
  struct _scoot_subsuite_node_t* next;
} scoot_subsuite_node_t;

typedef struct _scoot_suite_node_t {
  char* name;
  char* path;
  scoot_test_fn_t setup;
  scoot_test_fn_t cleanup;
  scoot_test_fn_t default_setup;
  scoot_test_fn_t default_cleanup;
  struct _scoot_suite_node_t* prev;
  struct _scoot_suite_node_t* next;
  struct _scoot_test_node_t* tests;
  struct _scoot_subsuite_node_t* subsuites;
  int bSeen;
} scoot_suite_node_t;

typedef struct _scoot_results_t {
  int numPass;
  int numExec;
} scoot_results_t;

typedef struct _scoot_suite_stack_t {
  struct _scoot_suite_node_t* node;
  struct _scoot_suite_stack_t* prev;
} scoot_suite_stack_t;

/*!
 * \brief Scans the specified file for SCOOT symbols, returning a new 
 * scoot_suite_node_t if any are found, or NULL otherwise.
 *
 * \param[in] fn Name of file to scan. If NULL, scans the current process.
 */
SCOOT_FN scoot_suite_node_t* scanFile( const char* fn );

/*!
 * Internal function to scan the specified path for test suites.
 *
 * \param[in] path Name of path to scan
 * \param[in] bRecurse Will recursively scan the specified path if not 0.
 */
SCOOT_FN void scanPath( const char* path, int bRecurse );

/*!
 * \brief Opens the specified SCOOT Test Suite file.
 *
 * Returns a handle to the loaded Test Suite or NULL on error.
 *
 * \param[in] fn The filename to load.
 */
SCOOT_FN scoot_suite_handle_t* loadTestSuite( const char* fn );

/*!
 * \brief Initializes the tokenization process for the given Test Suite
 * and returns the first token.
 *
 */
SCOOT_FN scoot_suite_token_t tokenizeTestSuiteFile( scoot_suite_handle_t* suite );

/*!
 * \brief Extracts the specified token from the specified Test Suite file and
 * retrieves the next token.
 *
 * The token's name is copied into the specified buffer.
 *
 * \param[in] suite A handle to the Test Suite.
 * \param[inout] token On input, this is the token to extract. On output this
 * is the next token in the stream, or NULL when done.
 * \param[out] buf The buffer to copy the token name into.
 * \param[in] buf_size The size of the buffer.
 * \return 0 on success, 1 on failure.
 */
SCOOT_FN int extractTestSuiteToken( scoot_suite_handle_t suite,
				    scoot_suite_token_t* token,
				    char* buf, int buf_size );

/*!
 * \brief Extracts the specified SCOOT function from the specified Test Suite
 * file.
 *
 * \param[in] suite A handle to the Test Suite.
 * \param[in] fnName The name of the function to extract.
 * \return NULL on error.
 */
SCOOT_FN scoot_test_fn_t extractTestSuiteFunction( scoot_suite_handle_t suite,
						   char* fnName );
/*!
 * \brief Closes the specified Test Suite.
 *
 * \param[in] suite The Test Suite to close.
 * \return 0 on success, 1 on failure.
 */
SCOOT_FN int closeTestSuite( scoot_suite_handle_t suite );

/*!
 * Updates the specified results with the specified test result.
 */
SCOOT_FN void runTest( scoot_test_node_t* node, scoot_results_t* results,
		       const char* ID, const char* suiteList );

/*!
 * Runs the specified test suite
 */
SCOOT_FN scoot_results_t* runSuite( scoot_suite_node_t* suite, const char* ID,
				    scoot_suite_stack_t* stack,
				    const char* suiteList);

/*!
 * Adds the specified newResults to the specified curResults.
 */
SCOOT_FN void updateResults( scoot_results_t* curResults,
			     scoot_results_t* newResults );

/*!
 * Prints a summary of the results.
 */
SCOOT_FN void printSummary( scoot_results_t* results );

/*!
 * Attempts to extract test function name and ID from the specified pString.
 *
 * \param[in] pStr PString to parse.
 * \param[out] pBuf Buffer to write name to.
 * \param[out] id Pointer to integer to copy ID to.
 * \param[in] bufSize Size of buffer. At most bufSize-1 characters plus 
 * terminating NULL will be written.
 * \return 0 on success, -1 on error.
 */
SCOOT_FN int extractTestFnName(char* pStr, char* pBuf, int* id,
			       int bufSize);

/*!
 * Attempts to extract setup function name from the specified pString.
 *
 * \param[in] pStr PString to parse.
 * \param[out] pBuf Buffer to write name to.
 * \param[in] bufSize Size of buffer. At most bufSize-1 characters plus 
 * terminating NULL will be written.
 * \return 0 on success, -1 on error.
 */
SCOOT_FN int extractSetupFnName(char* pStr, char* pBuf, int bufSize);

/*!
 * Attempts to extract cleanup function name from the specified pString.
 *
 * \param[in] pStr PString to parse.
 * \param[out] pBuf Buffer to write name to.
 * \param[in] bufSize Size of buffer. At most bufSize-1 characters plus 
 * terminating NULL will be written.
 * \return 0 on success, -1 on error.
 */
SCOOT_FN int extractCleanupFnName(char* pStr, char* pBuf, int bufSize);

/*!
 * Attempts to extract new suite function name from the specified pString.
 *
 * \param[in] pStr PString to parse.
 * \param[out] pBuf Buffer to write name to.
 * \param[in] bufSize Size of buffer. At most bufSize-1 characters plus 
 * terminating NULL will be written.
 * \return 0 on success, -1 on error.
 */
SCOOT_FN int extractNewSuiteFnName(char* pStr, char* pBuf, int bufSize);

/*!
 * Attempts to extract run suite function name and ID from the specified pString.
 *
 * \param[in] pStr PString to parse.
 * \param[out] pBuf Buffer to write name to.
 * \param[out] id Pointer to integer to copy ID to.
 * \param[in] bufSize Size of buffer. At most bufSize-1 characters plus 
 * terminating NULL will be written.
 * \return 0 on success, -1 on error.
 */
SCOOT_FN int extractRunSuiteFnName(char* pStr, char* pBuf, int* id,
				   int bufSize);

/*!
 * \brief Searches for the next available ID in the specified test suite,
 * given the specified starting ID.
 *
 * \param[in] suiteNode Suite node to search.
 * \param[in] id Starting ID
 * \return Node, if found. NULL otherwise.
 */
SCOOT_FN scoot_test_node_t* findNextAvailableID( scoot_suite_node_t* suiteNode,
						 int id );

/*!
 * \brief Generates a new test node with the specified name and ID.
 *
 * \param[in] name
 * \param[in] id
 * \return New test node, or NULL.
 */
SCOOT_FN scoot_test_node_t* makeTestNode( const char* name, int id );

/*!
 * \brief Properly destroys the specified test node.
 *
 * \param[in] node
 */
SCOOT_FN void destroyTestNode( scoot_test_node_t* node );

/*!
 * \brief Inserts the specified test node into the list of tests for the
 * specified suite node.
 *
 * Note: if specified suite node already has a test node with the same
 * ID, specified test node's ID will be incremented until a free ID
 * is found.
 *
 * \param[in] testNode Test node to insert
 * \param[in] suiteNode Suite node to update.
 */
SCOOT_FN void insertTestNode( scoot_test_node_t* testNode,
			      scoot_suite_node_t* suiteNode );

/*!
 * \brief Removes the specified test node from the list of tests for
 * the specified suite node.
 *
 * Node is *not* destroyed.
 *
 * \param[in] testNode Test node to remove
 * \param[in] suiteNode Suite node to update.
 */
SCOOT_FN void removeTestNode( scoot_test_node_t* testNode,
			      scoot_suite_node_t* suiteNode );

/*!
 * \brief Searches for the test node with the specified name in the
 * list of test nodes for the specified suite node.
 *
 * \param[in] suiteNode Suite node to search.
 * \param[in] name
 * \return Node, if found. NULL otherwise.
 */
SCOOT_FN scoot_test_node_t* findTestNodeByName( scoot_suite_node_t* suiteNode,
						char* name );

/*!
 * \brief Generates a new suite node with the specified name and ID.
 *
 * \param[in] name
 * \return New test node, or NULL.
 */
SCOOT_FN scoot_suite_node_t* makeSuiteNode( const char* name );

/*!
 * \brief Properly destroys the specified suite node.
 *
 * \param[in] node
 */
SCOOT_FN void destroySuiteNode( scoot_suite_node_t* node );

/*!
 * \brief Inserts the specified suite node into the global list of suites.
 *
 * \param[in] node Suite node to insert into global list of suites
 */
SCOOT_FN void insertSuiteNode( scoot_suite_node_t* node );

/*!
 * \brief Removes the specified suite node from the global list of suites.
 *
 * Node is properly disposed of.
 *
 * \param[in] node Suite node to remove from the global list of suites
 */
SCOOT_FN void removeSuiteNode( scoot_suite_node_t* node );

/*!
 * Attempts to extract suite setup function name from the specified pString.
 *
 * \param[in] pStr PString to parse.
 * \param[out] pBuf Buffer to write name to.
 * \param[in] bufSize Size of buffer. At most bufSize-1 characters plus 
 * terminating NULL will be written.
 * \return 0 on success, -1 on error.
 */
SCOOT_FN int extractSuiteSetupFnName(char* pStr, char* pBuf, int bufSize);

/*!
 * Attempts to extract suite cleanup function name from the specified pString.
 *
 * \param[in] pStr PString to parse.
 * \param[out] pBuf Buffer to write name to.
 * \param[in] bufSize Size of buffer. At most bufSize-1 characters plus 
 * terminating NULL will be written.
 * \return 0 on success, -1 on error.
 */
SCOOT_FN int extractSuiteCleanupFnName(char* pStr, char* pBuf, int bufSize);

/*!
 * Attempts to extract default setup function name from the specified pString.
 *
 * \param[in] pStr PString to parse.
 * \param[out] pBuf Buffer to write name to.
 * \param[in] bufSize Size of buffer. At most bufSize-1 characters plus 
 * terminating NULL will be written.
 * \return 0 on success, -1 on error.
 */
SCOOT_FN int extractDefaultSetupFnName(char* pStr, char* pBuf, int bufSize);

/*!
 * Attempts to extract default cleanup function name from the specified pString.
 *
 * \param[in] pStr PString to parse.
 * \param[out] pBuf Buffer to write name to.
 * \param[in] bufSize Size of buffer. At most bufSize-1 characters plus 
 * terminating NULL will be written.
 * \return 0 on success, -1 on error.
 */
SCOOT_FN int extractDefaultCleanupFnName(char* pStr, char* pBuf, int bufSize);

/*!
 * \brief Searches for the suite node with the specified name in the
 * global list of suite nodes.
 *
 * \param[in] name
 * \return Node, if found. NULL otherwise.
 */
SCOOT_FN scoot_suite_node_t* findSuiteNodeByName( char* name );

/*!
 * \brief Generates a new subsuite node with the specified name and ID.
 *
 * \param[in] name
 * \param[in] id
 * \return New subsuite node, or NULL.
 */
SCOOT_FN scoot_subsuite_node_t* makeSubsuiteNode( const char* name, int id );

/*!
 * \brief Properly destroys the specified subsuite node.
 *
 * \param[in] node
 */
SCOOT_FN void destroySubsuiteNode( scoot_suite_node_t* node );
/*!
 * \brief Inserts the specified suite into the list of subsuites for the
 * specified suite node.
 *
 * Note: if specified suite node already has a subsuite node with the same
 * ID, specified suite node's ID will be incremented until a free ID
 * is found.
 *
 * \param[in] name Name of suite to insert as a subsuite.
 * \param[in] id ID of subsuite.
 * \param[in] suiteNode Suite node to update.
 */
SCOOT_FN void insertSubsuiteNode( char* name, int id,
				  scoot_suite_node_t* suiteNode );

/*!
 * \brief Removes the specified subsuite node from the list of subsuites for
 * the specified suite node.
 *
 * Node is properly disposed of.
 *
 * \param[in] subNode Suite node to remove from list of subsuites
 * \param[in] suiteNode Suite node to update.
 */
SCOOT_FN void removeSubsuiteNode( scoot_suite_node_t* subNode,
				  scoot_suite_node_t* suiteNode );

/*!
 * \brief Searches for the suite node with the specified name in the
 * list of subsuite nodes for the specified suite node.
 *
 * \param[in] suiteNode Suite node to search.
 * \param[in] name
 * \return Node, if found. NULL otherwise.
 */
SCOOT_FN scoot_subsuite_node_t* findSubsuiteNodeByName( scoot_suite_node_t* suiteNode,
							char* name );

/*!
 * \breif Formats the specified message and routes to the results file.
 *
 * All \r and \n characters are stripped and replaced with a suitable value.
 * \addtogroup log_fns
 */
SCOOT_FN void scoot_log_results( scoot_log_msg_t msgType, const char* fmt, ... );

/*!
 * \brief Opens the log file.
 *
 * If fn is NULL, the default filename is used.
 * \addtogroup log_fns
 */
SCOOT_FN void scoot_start_log_file( const char* fn );

/*!
 * \brief Opens the results file.
 *
 * If fn is NULL, the default filename is used.
 * \addtogroup log_fns
 */
SCOOT_FN void scoot_start_results_file( const char* fn );

/*!
 * \brief Closes all open files.
 * \addtogroup log_fns
 */
SCOOT_FN void scoot_close_files();

/*!
 * \brief Reports success for the current test.
 * \addtogroup log_fns
 */
SCOOT_FN void scoot_report_test_pass( );

/*!
 * \brief Reports start of test
 * \addtogroup log_fns
 */
SCOOT_FN void scoot_report_test_start( const char* suiteID,
				       const char* suiteName,
				       int testID, const char* testName );

/*!
 * \brief Reports start of suite
 * \addtogroup log_fns
 */
SCOOT_FN void scoot_report_suite_start( const char* id, const char* suiteName );

/*!
 * \brief Reports success for the specified suite
 * \addtogroup log_fns
 */
SCOOT_FN void scoot_report_suite_results( const char* id, const char* suiteName,
					  int numPass, int numExec );

/*!
 * \brief Checks for unused suites.
 */
SCOOT_FN void scoot_scan_unused();

/*!
 * \brief Parses the commandline and returns a list of suite node names.
 *
 * Caller is responsible for disposing of returned values.
 */
SCOOT_FN scoot_subsuite_node_t* scoot_parse_commandline( int argc, char** argv );

/*!
 * \brief Displays the commandline help.
 */
SCOOT_FN void doHelp();

/*!
 * \brief Displays the version information.
 */
SCOOT_FN void doVersion();


#endif
