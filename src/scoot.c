/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Jeff Backus <jeff@jsbackus.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*! \file
 * Contains the implementations for various unit test helper functions
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

#include "scoot_internal.h"

static scoot_suite_node_t* _suites = NULL;
static FILE* _resultsFH = NULL;
static FILE* _logFH = NULL;

static int _bEnableDebugMsgs = 0;
static int _bVerboseMode = 0;
static int _bQuietMode = 0;

const int MAX_MSG_SIZE = 512;

SCOOT_FN scoot_suite_node_t* scanFile( const char* fn ) {

  if( fn == NULL ) {
    return NULL;
  }

  scoot_log_message( LOG_DEBUG, "Processing: %s", fn );

  /* Load suite file */
  scoot_suite_handle_t* suite = NULL;
  suite = loadTestSuite( fn );
  if( suite == NULL ) {
    return NULL;
  }

  /* Tokenize contents of file */
  scoot_suite_token_t token = tokenizeTestSuiteFile( suite );
  
  scoot_suite_node_t* pRetVal = makeSuiteNode(NULL);
  
  /* Copy path */
  int len = strlen(fn) + 1;
  pRetVal->path = (char*)malloc( sizeof(char) * len );
  CHECK_NEW( pRetVal->path );
  strncpy(pRetVal->path, fn, len);

  const int MAX_BUF_SIZE = 512;
  char tokenName[MAX_BUF_SIZE];
  char buf[MAX_BUF_SIZE];
  char tmpName[MAX_BUF_SIZE];
  int id = 0;
  scoot_suite_node_t* unmatchedFns = makeSuiteNode( NULL );
  scoot_test_node_t* node = NULL;
  while( token != NULL ) {
    if( extractTestSuiteToken( *suite, &token, &tokenName[0], MAX_BUF_SIZE ) == 0 ) {
      if( extractTestFnName( &tokenName[0], &buf[0], &id, 254 ) == 0 ) {
	scoot_log_message( LOG_DEBUG, "\t\t%s, ID: %d <test function>", buf, id);

	/* Find the next available ID, starting with the requested one */
	id = findNextAvailableID( pRetVal, id );
	
	/* Attempt to retrieve function pointer */
	scoot_test_fn_t fn = extractTestSuiteFunction( *suite, &tokenName[0] );
	
	if( fn != NULL ) {
	  /* See if we found any setup or cleanup functions for this test */
	  node = findTestNodeByName( unmatchedFns, &buf[0] );
	  
	  /* Create a new node and set name and ID */
	  if( node == NULL ) {
	    node = makeTestNode( &buf[0], id );
	  } else {
	    /* Remove node from unmatched list and add it to the list of 
	       tests for this suite. */
	    removeTestNode( node, unmatchedFns );
	    node->id = id;
	  }
	  
	  /* Set test field */
	  node->test = fn;
	  
	  insertTestNode( node, pRetVal );
	}
      } else if( extractSetupFnName( &tokenName[0], &buf[0], 254 ) == 0 ){
	scoot_log_message(LOG_DEBUG, "\t\t%s <setup function>", buf);
	
	scoot_test_fn_t fn = extractTestSuiteFunction( *suite, &tokenName[0] );
	
	/* Attempt to find the associated test node */
	node = findTestNodeByName( pRetVal, &buf[0] );
	
	/* Look for it in the list of unmatched setup/cleanup functions */
	if( node == NULL ) {
	  node = findTestNodeByName( unmatchedFns, &buf[0] );
	}
	
	if( node == NULL ) {
	  /* Create a new one if we have too and add it to the list of
	     unmatched functions*/
	  node = makeTestNode( &buf[0], -1 );
	  insertTestNode( node, unmatchedFns );	    
	}
	
	/* Update node with setup function. */
	node->setup = fn;
	
      } else if( extractCleanupFnName( &tokenName[0], &buf[0], 254 ) == 0 ){
	scoot_log_message(LOG_DEBUG, "\t\t%s <cleanup function>", buf);
	
	scoot_test_fn_t fn = extractTestSuiteFunction( *suite, &tokenName[0] );
	
	/* Attempt to find the associated test node */
	node = findTestNodeByName( pRetVal, &buf[0] );
	
	/* Look for it in the list of unmatched setup/cleanup functions */
	if( node == NULL ) {
	  node = findTestNodeByName( unmatchedFns, &buf[0] );
	}
	
	if( node == NULL ) {
	  /* Create a new one if we have too and add it to the list of
	     unmatched functions*/
	  node = makeTestNode( &buf[0], -1 );
	  insertTestNode( node, unmatchedFns );	    
	}
	
	/* Update node with cleanup function. */
	node->cleanup = fn;
	
      } else if( extractNewSuiteFnName( &tokenName[0], &buf[0], 254 ) == 0 ){
	scoot_log_message(LOG_DEBUG, "\t\t%s <new suite>", buf);
	
	if( pRetVal->name != NULL ) {
	  scoot_log_message(LOG_WARN, "Multiple suites defined in %s! Ignoring suite %s.",
			    pRetVal->path, buf);
	} else {
	  scoot_test_fn_t fn = extractTestSuiteFunction( *suite, &tokenName[0] );
	  
	  if( fn != NULL ) {
	    /* Execute */
	    fn( &tmpName[0], 255 );
	    if( strcmp( &tmpName[0], &buf[0] ) != 0 ) {
	      scoot_log_message(LOG_CRITICAL,
				"Run Suite function returned invalid name: '%s' != '%s'",
				buf, tmpName);
	      exit(1);
	    }
	    
	    /* Copy results into name field of current suite */
	    len = strlen( &tmpName[0] ) + 1;
	    pRetVal->name = (char*)malloc( sizeof(char) * len );
	    CHECK_NEW( pRetVal );
	    strncpy(pRetVal->name, &tmpName[0], len);
	    
	    /* Check to see if suite has been seen */
	    if( findSuiteNodeByName( &tmpName[0] ) != NULL ) {
	      return NULL;
	    }
	  }
	}
      } else if( extractRunSuiteFnName( &tokenName[0], &buf[0], &id, 254 ) == 0 ) {
	scoot_log_message(LOG_DEBUG, "\t\t%s, ID: %d <run suite>", buf, id);
	
	/* Find the next available ID, starting with the requested one */
	id = findNextAvailableID( pRetVal, id );

	scoot_test_fn_t fn = extractTestSuiteFunction( *suite, &tokenName[0] );
	
	if( fn != NULL ) {
	  /* Execute */
	  fn( &tmpName[0], 255 );
	  if( strcmp( &tmpName[0], &buf[0] ) != 0 ) {
	    scoot_log_message(LOG_CRITICAL, 
			      "Run Suite function returned invalid name: '%s' != '%s'",
			      buf, tmpName);
	    exit(1);
	  }
	  
	  /* Make sure suite doesn't already exist in this list */
	  if( findSubsuiteNodeByName( pRetVal, &tmpName[0] ) == NULL ) {
	    /* Insert into list of subsuites */
	    insertSubsuiteNode( &tmpName[0], id, pRetVal );
	  } else {
	    scoot_log_message(LOG_WARN, "Subsuites %s added multiple times in %s!",
			      buf, pRetVal->path);
	  }
	}
      } else if( extractSuiteSetupFnName( &tokenName[0], &buf[0], 254 ) == 0 ) {
	scoot_log_message(LOG_DEBUG, "\t\t%s <suite setup function>", buf);
	
	scoot_test_fn_t fn = extractTestSuiteFunction( *suite, &tokenName[0] );
	
	if( fn != NULL ) {
	  pRetVal->setup = fn;
	}
      } else if( extractSuiteCleanupFnName( &tokenName[0], &buf[0], 254 ) == 0 ) {
	scoot_log_message(LOG_DEBUG, "\t\t%s <suite setup function>", buf);
	
	scoot_test_fn_t fn = extractTestSuiteFunction( *suite, &tokenName[0] );
	
	if( fn != NULL ) {
	  pRetVal->cleanup = fn;
	}
      } else if( extractDefaultSetupFnName( &tokenName[0], &buf[0], 254 ) == 0 ) {
	scoot_log_message(LOG_DEBUG, "\t\t%s <default setup function>", buf);
	
	scoot_test_fn_t fn = extractTestSuiteFunction( *suite, &tokenName[0] );
	
	if( fn != NULL ) {
	  pRetVal->default_setup = fn;
	}
      } else if( extractDefaultCleanupFnName( &tokenName[0], &buf[0], 254 ) == 0 ) {
	scoot_log_message(LOG_DEBUG, "\t\t%s <default setup function>", buf);
	
	scoot_test_fn_t fn = extractTestSuiteFunction( *suite, &tokenName[0] );
	
	if( fn != NULL ) {
	  pRetVal->default_cleanup = fn;
	}
      }
    } else {
      scoot_log_message( LOG_ERROR, "Unknown error extracting next token." );
      token = NULL;
    }
  }
  
  /* Check to see if we have any unmatched functions. If so, alert user and
     clean up. */
  node = unmatchedFns->tests;
  while( node != NULL ) {
    if( node->setup != NULL ) {
      scoot_log_message( LOG_ERROR,
			 "Unmatched setup function for test '%s'.",
			 node->name);
    }
    if( node->cleanup != NULL ) {
      scoot_log_message(LOG_ERROR,
			"Unmatched cleanup function for test '%s'.",
			node->name);
    }
    scoot_test_node_t* pTmp = node;
    node = node->next;
    destroyTestNode(pTmp);
    pTmp = NULL;
  }

  /* Free object if it isn't valid */
  if( pRetVal->name == NULL ) {
    destroySuiteNode( pRetVal );
  }

  /* Add to global list */
  insertSuiteNode( pRetVal );
  
  return pRetVal;
}

SCOOT_FN void runTest( scoot_test_node_t* node, scoot_results_t* results, const char* ID,
	      const char* suiteList ) {
  if( node == NULL || node->test == NULL || results == NULL ||
      suiteList == NULL ) {
    
    return;
  }

  scoot_report_test_start(ID, suiteList, node->id, node->name );

  results->numExec++;

  int setupFailed = 0;
  int runFailed = 1;
  int cleanupFailed = 0;
  
  /* Run Setup, if specified */
  if( node->setup != NULL ) {
    setupFailed = node->setup();
  }

  /* Run Test */
  if( setupFailed == 0 ) {
    runFailed = node->test();
  }

  /* Run Cleanup if specified */
  if( node->cleanup != NULL ) {
    cleanupFailed = node->cleanup();
  }

  if( setupFailed != 0 || cleanupFailed != 0 ) {
    if( cleanupFailed == 0 ) {
      scoot_log_message( LOG_ERROR, "Test setup failed.\n" );
    } else if( setupFailed == 0 ) {
      scoot_log_message( LOG_ERROR, "Test cleanup failed.\n" );
    } else {
      scoot_log_message( LOG_ERROR, "Test setup failed. Test cleanup Failed.\n" );
    }
  } else {
    if( runFailed == 0 ) {
      scoot_report_test_pass( );
      results->numPass++;
    }
  }
}

SCOOT_FN scoot_results_t* runSuite( scoot_suite_node_t* suite, const char* ID,
				    scoot_suite_stack_t* stack,
				    const char* suiteList ) {
  if( suite == NULL ) {
    return NULL;
  }

  const int MAX_SUITE_LIST_SIZE = 512;
  char lclSuiteList[MAX_SUITE_LIST_SIZE];

  if( suiteList != NULL ) {
    snprintf( &lclSuiteList[0], MAX_SUITE_LIST_SIZE, "%s.%s", suiteList,
	      suite->name);
  } else {
    snprintf( &lclSuiteList[0], MAX_SUITE_LIST_SIZE, "%s", suite->name);
  }
  

  scoot_results_t* retVal = (scoot_results_t*)malloc(sizeof(scoot_results_t));
  CHECK_NEW( retVal );

  scoot_report_suite_start( ID, lclSuiteList );

  retVal->numPass = 0;
  retVal->numExec = 0;

  /* Mark suite node seen */
  suite->bSeen = 1;

  int hasError = 0;

  /* Scan the stack to see if this node is on it. */
  scoot_suite_stack_t* pTmpStack = stack;
  while( pTmpStack != NULL ) {
    if( pTmpStack->node == suite ) {
      scoot_log_message( LOG_ERROR, "Circular dependency detected!\n" );
      retVal->numExec = 1;
      hasError = 1;
    }
    pTmpStack = pTmpStack->prev;
  }

  if( hasError == 0 ) {
    /* Attempt to register callbacks */
    scoot_suite_handle_t* hSuite = loadTestSuite( suite->path );
    scoot_register_callbacks_fn_t callbackFn = (scoot_register_callbacks_fn_t)
      extractTestSuiteFunction( *hSuite, "_SCOOT_REGISTER_CALLBACKS" );
    if( callbackFn == NULL ) {
      scoot_log_message( LOG_ERROR, "Test Suite lacks callback registration function. Skipping!");
      retVal->numExec = 1;
      return retVal;
    }
    callbackFn( &scoot_log_message, &scoot_report_test_fail, &scoot_stringify_compare );
    
    if( suite->setup != NULL ) {
      if( suite->setup() != 0 ) {
	scoot_log_message( LOG_ERROR, "Suite setup function failed.\n" );
	retVal->numExec = 1;
	hasError = 1;
      }
    }
  }

  if( hasError == 0 ) {
    /* Push this node onto the call stack. */
    scoot_suite_stack_t lclStack;
    lclStack.prev = stack;
    lclStack.node = suite;
    
    /* Run Tests and Subsuites */
    scoot_test_node_t* curTest = suite->tests;
    scoot_subsuite_node_t* curSubsuite = suite->subsuites;
    int bDone = 0;
    char buf[256];
    while( !bDone ) {
      bDone = 1;

      if( curTest != NULL && (curSubsuite == NULL ||
			      curTest->id < curSubsuite->id) ) {

	/** Run Test **/
	/* Update test node with default setup/cleanup functions if necessary */
	if( curTest->setup == NULL ) {
	  curTest->setup = suite->default_setup;
	}
	if( curTest->cleanup == NULL ) {
	  curTest->cleanup = suite->default_cleanup;
	}
      
	runTest( curTest, retVal, ID, lclSuiteList );
	
	curTest = curTest->next;
	bDone = 0;
      }

      if( curSubsuite != NULL && (curTest == NULL ||
				  curSubsuite->id < curTest->id) ) {

	/** Run Subsuite **/

	/* Prepare ID for subsuite */
	snprintf(&buf[0], 255, "%s.%0d", ID, curSubsuite->id );
	scoot_suite_node_t* node = findSuiteNodeByName( curSubsuite->name );
	
	/* Check for nonexistant suite */
	if( node == NULL ) {
	  scoot_log_message( LOG_ERROR, "Unable to find test suite '%s'.",
			     curSubsuite->name );
	  retVal->numExec++;
	} else {
	  scoot_results_t* results = runSuite( node, &buf[0], &lclStack,
					       &lclSuiteList[0] );
	  updateResults( retVal, results );
	}
	curSubsuite = curSubsuite->next;
	bDone = 0;
      }
    }
  }

  if( suite->cleanup != NULL ) {
    if( suite->cleanup() != 0 ) {
      scoot_log_message( LOG_ERROR, "Suite cleanup function failed.");
      retVal->numExec++;
      hasError = 1;
    }
  }

  scoot_report_suite_results( ID, &lclSuiteList[0], retVal->numPass,
			      retVal->numExec );
  return retVal;
}

SCOOT_FN void updateResults( scoot_results_t* curResults,
			     scoot_results_t* newResults ) {
  if( curResults == NULL || newResults == NULL ) {
    return;
  }
  curResults->numPass += newResults->numPass;
  curResults->numExec += newResults->numExec;
}

void printSummary( scoot_results_t* results ) {
  if( results == NULL ) {
    return;
  }
  scoot_log_message(LOG_FINAL_RESULTS, "\n");
  scoot_log_message(LOG_FINAL_RESULTS,
		    "------------------------------------------------\n");
  scoot_log_message(LOG_FINAL_RESULTS, "Testing complete: %0d of %0d passed!\n\n",
		    results->numPass, results->numExec);
}

SCOOT_FN int extractTestFnName(char* pStr, char* pBuf, int* id,
			       int bufSize) {
  
  static const char* testSig = "_SCOOT_TEST_FN_";
  static int testSigLen = 0;
  if( testSigLen == 0 ) {
    testSigLen = strlen(testSig);
  }

  if( 0 == strncmp( pStr, testSig, testSigLen) ) {
    char* pID = pStr + testSigLen;
    char* pName = pID;
    while( *pName != '_' && *pName != 0 ) {
      pName++;
    }

    int len = (pName - pID) + 1;
    if( bufSize - 1 < len ) {
      len = bufSize - 1;
    }
    strncpy(pBuf, pID, len);
    pBuf[len] = 0;
    *id = atoi(pBuf);
	  
    pName++;
    strncpy(pBuf, pName, bufSize - 1);
    pBuf[bufSize - 1] = 0;

    return 0;
  }
  return -1;
}

SCOOT_FN int extractSetupFnName(char* pStr, char* pBuf, int bufSize) {
  
  static const char* setupSig = "_SCOOT_SETUP_FN_";
  static int setupSigLen = 0;
  if( setupSigLen == 0 ) {
    setupSigLen = strlen(setupSig);
  }

  if( 0 == strncmp( pStr, setupSig, setupSigLen) ) {
    char* pName = pStr + setupSigLen;
    
    strncpy(pBuf, pName, bufSize - 1);
    pBuf[bufSize - 1] = 0;

    return 0;
  }
  return -1;
}

SCOOT_FN int extractCleanupFnName(char* pStr, char* pBuf, int bufSize) {

  const char* cleanupSig = "_SCOOT_CLEANUP_FN_";
  static int cleanupSigLen = 0;
  if( cleanupSigLen == 0 ) {
    cleanupSigLen = strlen(cleanupSig);
  }

  if( 0 == strncmp( pStr, cleanupSig, cleanupSigLen) ) {
    char* pName = pStr + cleanupSigLen;
    
    strncpy(pBuf, pName, bufSize - 1);
    pBuf[bufSize - 1] = 0;
    
    return 0;
  }
  return -1;
}

SCOOT_FN int extractNewSuiteFnName(char* pStr, char* pBuf, int bufSize) {

  static const char* defSuiteSig = "_SCOOT_TEST_SUITE_";
  static int defSuiteSigLen = 0;
  if( defSuiteSigLen == 0 ) {
    defSuiteSigLen = strlen(defSuiteSig);
  }

  if( 0 == strncmp( pStr, defSuiteSig, defSuiteSigLen) ) {
    char* pName = pStr + defSuiteSigLen;

    strncpy(pBuf, pName, bufSize - 1);
    pBuf[bufSize - 1] = 0;
    
    return 0;
  }
  return -1;
}

SCOOT_FN int extractRunSuiteFnName(char* pStr, char* pBuf, int* id,
				   int bufSize) {

  static const char* runSuiteSig = "_SCOOT_RUN_SUITE_";
  static int runSuiteSigLen = 0;
  if( runSuiteSigLen == 0 ) {
    runSuiteSigLen = strlen(runSuiteSig);
  }

  if( 0 == strncmp( pStr, runSuiteSig, runSuiteSigLen) ) {
    char* pID = pStr + runSuiteSigLen;
    char* pName = pID;
    while( *pName != '_' && *pName != 0 ) {
      pName++;
    }

    int len = (pName - pID) + 1;
    if( bufSize - 1 < len ) {
      len = bufSize - 1;
    }
    strncpy(pBuf, pID, len);
    pBuf[len] = 0;
    *id = atoi(pBuf);
	  
    pName++;
    strncpy(pBuf, pName, bufSize - 1);
    pBuf[bufSize - 1] = 0;

    return 0;
  }
  return -1;
}

SCOOT_FN scoot_test_node_t* findNextAvailableID( scoot_suite_node_t* suiteNode,
						 int id ) {

  scoot_test_node_t* pTmpTest = suiteNode->tests;
  scoot_subsuite_node_t* pTmpSuite = suiteNode->subsuites;

  int bDone = 0;

  /* Scan test and subsuite IDs in parallel. */
  while( !bDone ) {
    bDone = 1;

    /* Check current test ID */
    if( pTmpTest != NULL ) {
      /* If equal, increment our temporary ID */
      if( id == pTmpTest->id ) {
	id++;
	bDone = 0;
      }
      /* If our temporary ID is greater than the current test ID, move to the 
	 next test. */
      if( pTmpTest->id < id ) {
	pTmpTest = pTmpTest->next;
	bDone = 0;
      }
    }

    /* Check current suite ID */
    if( pTmpSuite != NULL ) {
      /* If equal, increment our temporary ID */
      if( id == pTmpSuite->id ) {
	id++;
	bDone = 0;
      }
      /* If our temporary ID is greater than the current suite ID, move to the 
	 next suite. */
      if( pTmpSuite->id < id ) {
	pTmpSuite = pTmpSuite->next;
	bDone = 0;
      }
    }
  }
  
  return id;
}

SCOOT_FN scoot_test_node_t* makeTestNode( const char* name, int id ) {
  if( name == NULL ) {
    return NULL;
  }

  /* Create a new node */
  scoot_test_node_t* node =
    (scoot_test_node_t*)malloc(sizeof(scoot_test_node_t));
  CHECK_NEW( node );

  /* Set name field */
  int len = strlen(name) + 1;
  node->name = (char*)malloc(sizeof(char) * len);
  CHECK_NEW( node->name );
  strncpy( node->name, name, len );
  
  /* Set id field */
  node->id = id;

  /* Set next and prev */
  node->next = NULL;
  node->prev = NULL;

  node->setup = NULL;
  node->cleanup = NULL;

  return node;
}

SCOOT_FN void destroyTestNode( scoot_test_node_t* node ) {
  if( node == NULL ) {
    return;
  }

  if( node->name != NULL ) {
    free( node->name );
  }
  node->next = NULL;
  node->prev = NULL;
  node->setup = NULL;
  node->test = NULL;
  node->cleanup = NULL;
  node->id = -1;
}

SCOOT_FN void insertTestNode( scoot_test_node_t* testNode,
			      scoot_suite_node_t* suiteNode ) {

  if( testNode == NULL || suiteNode == NULL ) {
    return;
  }

  /* Keep the list of tests ordered by ID. When we find a duplicate,
     increment until no more duplicates.
  */
  int id = testNode->id;
  scoot_test_node_t* pTmp = suiteNode->tests;

  if( pTmp == NULL ) {
    suiteNode->tests = testNode;
    testNode->prev = NULL;
    testNode->next = NULL;
    return;
  }

  /* Compare ID against top of list. */
  if( id < pTmp->id ) {
    suiteNode->tests = testNode;
    testNode->prev = NULL;
    testNode->next = pTmp;
    pTmp->prev = testNode;
    return;
  }

  if( id == pTmp->id ) {
    id++;
  }

  /* Walk */
  while( pTmp->next != NULL && pTmp->next->id <= id ) {
    if( pTmp->next->id == id ) {
      id++;
    }
    pTmp = pTmp->next;
  }

  /* Insert */
  testNode->prev = pTmp;
  testNode->next = pTmp->next;
  
  if( pTmp->next != NULL ) {
    pTmp->next->prev = testNode;
  }
  pTmp->next = testNode;

  testNode->id = id;
}

SCOOT_FN void removeTestNode( scoot_test_node_t* testNode,
			      scoot_suite_node_t* suiteNode ) {
  if( testNode == NULL || suiteNode == NULL ) {
    return;
  }

  /* Check head of list */
  if( suiteNode->tests == testNode ) {
    suiteNode->tests = testNode->next;
  }

  /* Remove from list */
  if( testNode->prev != NULL ) {
    testNode->prev->next = testNode->next;
  }
  if( testNode->next != NULL ) {
    testNode->next->prev = testNode->prev;
  }
  testNode->next = NULL;
  testNode->prev = NULL;
}

SCOOT_FN scoot_test_node_t* findTestNodeByName( scoot_suite_node_t* suiteNode,
						char* name ) {
  if( suiteNode == NULL || name == NULL || suiteNode->tests == NULL ) {
    return NULL;
  }

  /* Grab the head of the test list. */
  scoot_test_node_t* head = suiteNode->tests;
  scoot_test_node_t* pTmp = head;

  /* Start looking down the 'next' path */
  while( pTmp != NULL ) {
    if( pTmp->name != NULL && strcmp(pTmp->name, name) == 0 ) {
      return pTmp;
    }
    pTmp = pTmp->next;
  }

  /* Didn't find it. Look down the 'prev' path */
  pTmp = head->prev;
  while( pTmp != NULL ) {
    if( pTmp->name != NULL && strcmp(pTmp->name, name) == 0 ) {
      return pTmp;
    }
    pTmp = pTmp->prev;
  }

  /* Still no dice. Return NULL. */
  return NULL;
}

SCOOT_FN scoot_suite_node_t* makeSuiteNode( const char* name ) {
  scoot_suite_node_t* pRetVal =
    (scoot_suite_node_t*)malloc(sizeof(scoot_suite_node_t));
  CHECK_NEW( pRetVal );
  if( name != NULL ) {
    int len = strlen( name ) + 1;
    pRetVal->name = (char*)malloc(sizeof(char) * len);
    CHECK_NEW(pRetVal->name);
    strncpy(pRetVal->name, name, len );
  } else {
    pRetVal->name = NULL;
  }
  pRetVal->path = NULL;
  pRetVal->prev = NULL;
  pRetVal->next = NULL;
  pRetVal->tests = NULL;
  pRetVal->subsuites = NULL;
  pRetVal->setup = NULL;
  pRetVal->cleanup = NULL;
  pRetVal->default_setup = NULL;
  pRetVal->default_cleanup = NULL;
  pRetVal->bSeen = 0;

  return pRetVal;
}

SCOOT_FN void destroySuiteNode( scoot_suite_node_t* node ) {
  /* TODO */
  fprintf(stderr, "Not Implemented Yet! Line %0d, file %s", __LINE__, __FILE__);
}

SCOOT_FN void insertSuiteNode( scoot_suite_node_t* node ) {

  if( node == NULL ) {
    return;
  }

  if( _suites != NULL ) {
    _suites->next = node;
  }
  
  node->prev = _suites;
  _suites = node;
}

SCOOT_FN void removeSuiteNode( scoot_suite_node_t* node ) {
  /* TODO */
  fprintf(stderr, "Not Implemented Yet! Line %0d, file %s", __LINE__, __FILE__);
}

SCOOT_FN scoot_suite_node_t* findSuiteNodeByName( char* name ) {
  if( _suites == NULL || name == NULL ) {
    return NULL;
  }

  scoot_suite_node_t* pTmp = _suites;

  /* Start looking down the 'next' path */
  while( pTmp != NULL ) {
    if( pTmp->name != NULL && strcmp(pTmp->name, name) == 0 ) {
      return pTmp;
    }
    pTmp = pTmp->next;
  }

  /* Didn't find it. Look down the 'prev' path */
  pTmp = _suites->prev;
  while( pTmp != NULL ) {
    if( pTmp->name != NULL && strcmp(pTmp->name, name) == 0 ) {
      return pTmp;
    }
    pTmp = pTmp->prev;
  }

  /* Still no dice. Return NULL. */
  return NULL;
}

SCOOT_FN scoot_subsuite_node_t* makeSubsuiteNode( const char* name, int id ) {

  /* Create a new node */
  scoot_subsuite_node_t* node =
    (scoot_subsuite_node_t*)malloc(sizeof(scoot_subsuite_node_t));
  CHECK_NEW( node );

  /* Copy results into new node's name field and insert into list of
   * subsuites. */
  int len = strlen(name) + 1;
  node->name = (char*)malloc(sizeof(char) * len);
  CHECK_NEW( node->name );
  strncpy( node->name, name, len );

  /* Set id */
  node->id = id;

  /* Set other parameters */
  node->next = NULL;
  node->prev = NULL;

  return node;
}

SCOOT_FN void destroySubsuiteNode( scoot_suite_node_t* node ) {
  /* TODO */
  fprintf(stderr, "Not Implemented Yet! Line %0d, file %s", __LINE__, __FILE__);
}

SCOOT_FN void insertSubsuiteNode( char* name, int id,
				  scoot_suite_node_t* suiteNode ) {

  if( name == NULL || suiteNode == NULL ) {
    return;
  }

  /* Create a new node */
  scoot_subsuite_node_t* subNode = makeSubsuiteNode( name, id );

  /* Keep the list of subsuites ordered by ID. When we find a duplicate,
     increment until no more duplicates.
  */

  scoot_subsuite_node_t* pTmp = suiteNode->subsuites;

  if( pTmp == NULL ) {
    suiteNode->subsuites = subNode;
    subNode->prev = NULL;
    subNode->next = NULL;
    return;
  }

  /* Compare ID against top of list. */
  if( id < pTmp->id ) {
    suiteNode->subsuites = subNode;
    subNode->prev = NULL;
    subNode->next = pTmp;
    pTmp->prev = subNode;
    return;
  }

  if( id == pTmp->id ) {
    id++;
  }

  /* Walk */
  while( pTmp->next != NULL && pTmp->next->id <= id ) {
    if( pTmp->next->id == id ) {
      id++;
    }
    pTmp = pTmp->next;
  }

  /* Insert */
  subNode->prev = pTmp;
  subNode->next = pTmp->next;
  
  if( pTmp->next != NULL ) {
    pTmp->next->prev = subNode;
  }
  pTmp->next = subNode;

  subNode->id = id;
}

SCOOT_FN void removeSubsuiteNode( scoot_suite_node_t* subNode,
				  scoot_suite_node_t* suiteNode ) {
  /* TODO */
  fprintf(stderr, "Not Implemented Yet! Line %0d, file %s", __LINE__, __FILE__);
}

SCOOT_FN scoot_subsuite_node_t* findSubsuiteNodeByName( scoot_suite_node_t* suiteNode,
							char* name ) {
  if( suiteNode == NULL || name == NULL || suiteNode->subsuites == NULL ) {
    return NULL;
  }

  /* Grab the head of the subsuite list. */
  scoot_subsuite_node_t* head = suiteNode->subsuites;
  scoot_subsuite_node_t* pTmp = head;

  /* Start looking down the 'next' path */
  while( pTmp != NULL ) {
    if( pTmp->name != NULL && strcmp(pTmp->name, name) == 0 ) {
      return pTmp;
    }
    pTmp = pTmp->next;
  }

  /* Didn't find it. Look down the 'prev' path */
  pTmp = head->prev;
  while( pTmp != NULL ) {
    if( pTmp->name != NULL && strcmp(pTmp->name, name) == 0 ) {
      return pTmp;
    }
    pTmp = pTmp->prev;
  }

  /* Still no dice. Return NULL. */
  return NULL;
}

SCOOT_FN int extractSuiteSetupFnName(char* pStr, char* pBuf, int bufSize) {

  const char* sig = "_SCOOT_SUITE_SETUP_FN";
  static int sigLen = 0;
  if( sigLen == 0 ) {
    sigLen = strlen(sig);
  }

  if( 0 == strncmp( pStr, sig, sigLen) ) {
    char* pName = pStr + sigLen;
    
    strncpy(pBuf, pName, bufSize - 1);
    pBuf[bufSize - 1] = 0;
    
    return 0;
  }
  return -1;
}

SCOOT_FN int extractSuiteCleanupFnName(char* pStr, char* pBuf, int bufSize) {

  const char* sig = "_SCOOT_SUITE_CLEANUP_FN";
  static int sigLen = 0;
  if( sigLen == 0 ) {
    sigLen = strlen(sig);
  }

  if( 0 == strncmp( pStr, sig, sigLen) ) {
    char* pName = pStr + sigLen;
    
    strncpy(pBuf, pName, bufSize - 1);
    pBuf[bufSize - 1] = 0;
    
    return 0;
  }
  return -1;
}

SCOOT_FN int extractDefaultSetupFnName(char* pStr, char* pBuf, int bufSize) {

  const char* sig = "_SCOOT_DEFAULT_SETUP_FN";
  static int sigLen = 0;
  if( sigLen == 0 ) {
    sigLen = strlen(sig);
  }

  if( 0 == strncmp( pStr, sig, sigLen) ) {
    char* pName = pStr + sigLen;
    
    strncpy(pBuf, pName, bufSize - 1);
    pBuf[bufSize - 1] = 0;
    
    return 0;
  }
  return -1;
}

SCOOT_FN int extractDefaultCleanupFnName(char* pStr, char* pBuf, int bufSize) {

  const char* sig = "_SCOOT_DEFAULT_CLEANUP_FN";
  static int sigLen = 0;
  if( sigLen == 0 ) {
    sigLen = strlen(sig);
  }

  if( 0 == strncmp( pStr, sig, sigLen) ) {
    char* pName = pStr + sigLen;
    
    strncpy(pBuf, pName, bufSize - 1);
    pBuf[bufSize - 1] = 0;
    
    return 0;
  }
  return -1;
}

SCOOT_FN void scoot_start_log_file( const char* fn ) {
  if( _logFH != NULL ) {
    fclose( _logFH );
    _logFH = NULL;
  }

  const char defaultFN[] = "scoot.log";
  const char* pFN;
  if( fn != NULL ) {
    pFN = fn;
  } else {
    pFN = &defaultFN[0];
  }

  _logFH = fopen( pFN, "w" );
  if( _logFH == NULL ) {
    scoot_log_message( LOG_ERROR, "Unable to open log file '%s'!\n\n", pFN );
  }
}

SCOOT_FN void scoot_start_results_file( const char* fn ) {
  if( _resultsFH != NULL ) {
    fclose( _resultsFH );
    _resultsFH = NULL;
  }

  const char defaultFN[] = "scoot_results.txt";
  const char* pFN;
  if( fn != NULL ) {
    pFN = fn;
  } else {
    pFN = &defaultFN[0];
  }

  _resultsFH = fopen( pFN, "w" );
  if( _resultsFH == NULL ) {
    scoot_log_message( LOG_ERROR, "Unable to open results file '%s'!\n\n", pFN );
  }
}

SCOOT_FN void scoot_close_files() {
  if( _logFH != NULL ) {
    fclose( _logFH );
    _logFH = NULL;
  }

  if( _resultsFH != NULL ) {
    fclose( _resultsFH );
    _resultsFH = NULL;
  }
}

SCOOT_FN void scoot_log_message( scoot_log_msg_t msgType, const char* fmt, ... ) {
  char msg[MAX_MSG_SIZE];

  char forHuman[MAX_MSG_SIZE];
  forHuman[0] = (char)NULL;

  /* Convert varargs to a string */
  va_list args;
  va_start( args, fmt );
  vsnprintf( &msg[0], MAX_MSG_SIZE, fmt, args );
  va_end( args );

  /* Figure out what to do based on type */
  switch( msgType ) {
  case LOG_PASS:
  case LOG_FAIL:
  case LOG_TEST:
  case LOG_SUITE:
  case LOG_SUITE_RESULTS:
  case LOG_FINAL_RESULTS:
    if( _bQuietMode == 0 ) {
      snprintf(&forHuman[0], MAX_MSG_SIZE, "%s", msg);
    }
    break;
  case LOG_ERROR:
    snprintf(&forHuman[0], MAX_MSG_SIZE, "\n[Error] %s\n", msg);
    break;
  case LOG_WARN:
    snprintf(&forHuman[0], MAX_MSG_SIZE, "\n[Warning] %s\n", msg);
    break;
  case LOG_INFO:
    if( _bVerboseMode == 1 ) {
      snprintf(&forHuman[0], MAX_MSG_SIZE, "[Info] %s\n", msg);
    }
    break;
  case LOG_DEBUG:
    if( _bEnableDebugMsgs == 1 ) {
      snprintf(&forHuman[0], MAX_MSG_SIZE, "[Debug] %s\n", msg);
    }
    break;
  case LOG_CRITICAL:
    snprintf(&forHuman[0], MAX_MSG_SIZE, "\n[Critical] %s\n", msg);
    break;
  default:
    fprintf(stderr, "Error: Invalid message type %0d: '%s'\n", msgType, msg );
    break;
  }

  if( forHuman[0] != NULL ) {
    if( msgType != LOG_CRITICAL ) {
      printf( "%s", forHuman );
      fflush( stdout );
    } else {
      fprintf( stderr, "%s", forHuman );
    }

    if( _logFH != NULL ) {
      fprintf( _logFH, "%s", forHuman );
    }
    fflush( _logFH );
  }

  if( msgType == LOG_CRITICAL ) {
    scoot_close_files();
  }
}

SCOOT_FN void scoot_log_results( scoot_log_msg_t msgType, const char* fmt, ... ) {
  char msg[MAX_MSG_SIZE];

  char* pResultsStr = NULL;
  char forResults[MAX_MSG_SIZE];
  forResults[0] = (char)NULL;

  /* Convert varargs to a string */
  va_list args;
  va_start( args, fmt );
  vsnprintf( &msg[0], MAX_MSG_SIZE, fmt, args );
  va_end( args );

  /* Figure out what to do based on type */
  switch( msgType ) {
  case LOG_PASS:
  case LOG_FAIL:
  case LOG_TEST:
  case LOG_SUITE_RESULTS:
    strncpy( &forResults[0], &msg[0], MAX_MSG_SIZE );
    break;
  case LOG_SUITE:
    break;
  default:
    fprintf(stderr, "Error: Invalid message type %0d: '%s'\n", msgType, msg );
    break;
  }
  
  if( forResults[0] != NULL && _resultsFH != NULL ) {
    /* Strip out newlines (except last one) and tabs from forResults msg */
    pResultsStr = &forResults[0];
    while( *pResultsStr != (char)NULL ) {
      if( ((*pResultsStr == '\n' || *pResultsStr == '\r') && pResultsStr[1] != NULL)
	  || *pResultsStr == '\t' ) {
	*pResultsStr = ' ';
      }
      pResultsStr++;
    }

    fprintf( _resultsFH, "%s", forResults );
    fflush( _resultsFH );
  } 
}

SCOOT_FN void scoot_report_test_start( const char* suiteID,
				       const char* suiteName,
				       int testID, const char* testName ) {
  scoot_log_message( LOG_TEST, "%s.%0d %s...", suiteID, testID, testName );
  scoot_log_results( LOG_TEST, "%s.%0d|%s|%s|", suiteID, testID, suiteName,
		     testName );
}

SCOOT_FN void scoot_report_test_pass( ) {
  scoot_log_message( LOG_PASS, " Pass\n" );
  scoot_log_results( LOG_PASS, "Pass|\n" );
}

SCOOT_FN void scoot_report_test_fail( const char* filename, int line, const char* fmt, ... ) {
  char msg[MAX_MSG_SIZE];

  /* Convert varargs to a string */
  va_list args;
  va_start( args, fmt );
  vsnprintf( &msg[0], MAX_MSG_SIZE, fmt, args );
  va_end( args );

  scoot_log_message( LOG_FAIL, " %s\n\t\tin file %s, line %0d.\n", msg,
		     filename, line );

  scoot_log_results( LOG_FAIL, "Fail|%s in file %s, line %0d.\n", msg,
		     filename, line );

}

SCOOT_FN void scoot_report_suite_start( const char* id, const char* suiteName ) {
  scoot_log_message( LOG_SUITE, "%s %s:\n", id, suiteName );
}

SCOOT_FN void scoot_report_suite_results( const char* id, const char* suiteName,
					  int numPass, int numExec ) {
  char passResults[] = "Pass";
  char failResults[] = "Fail";

  char* pResultsStr = &passResults[0];

  scoot_log_message( LOG_SUITE_RESULTS, "%s %s: %d of %d passed\n\n",
		     id, suiteName, numPass, numExec );

  if( numPass < numExec ) {
    pResultsStr = &failResults[0];
  }
  scoot_log_results( LOG_SUITE_RESULTS, "%s|%s||%s|%d of %d passed\n",
		     id, suiteName, pResultsStr, numPass, numExec);
}


SCOOT_FN void scoot_scan_unused() {
  scoot_suite_node_t* node = _suites;
  int unusedCount = 0;
  while( node != NULL ) {
    if( node->bSeen == 0 ) {
      scoot_log_message( LOG_WARN, "Suite %s not used!", node->name );
      unusedCount++;
    }
    node = node->prev;
  }
  if( 0 < unusedCount ) {
    scoot_log_message( LOG_WARN, "There were %d unused suites.", unusedCount );
  }
}

SCOOT_FN scoot_subsuite_node_t* scoot_parse_commandline( int argc, char** argv ) {
  scoot_subsuite_node_t* retVal = NULL;
  scoot_subsuite_node_t* pNode = NULL;
  int i = 1;
  int nodeNum = 1;

  int bPathSpecified = 0;
  int bRecurse = 0;
  
  while( i < argc ) {
    if( strcmp(argv[i], "-d" ) == 0 ) {
      _bEnableDebugMsgs = 1;
      _bVerboseMode = 1;
      _bQuietMode = 0;
      i++;
    } else if( strcmp(argv[i], "-q" ) == 0 ) {
      _bQuietMode = 1;
      _bEnableDebugMsgs = 0;
      _bVerboseMode = 0;
      i++;
    } else if( strcmp(argv[i], "-v" ) == 0 ) {
      _bVerboseMode = 1;
      _bQuietMode = 0;
      i++;
    } else if( strcmp(argv[i], "--version" ) == 0 ) {
      doVersion();
      exit(0);
    } else if( strcmp(argv[i], "-r" ) == 0 ) {
      bRecurse = 1;
      i++;
    } else if( strcmp(argv[i], "-h" ) == 0 ) {
      doHelp();
      exit(0);
    } else if( strcmp(argv[i], "-p" ) == 0 ) {
      bPathSpecified = 1;
      if( i < argc - 1 ) {
	scanPath( argv[i + 1], bRecurse );
      } else {
	scoot_log_message( LOG_ERROR, "Option -p requires a path." );
	doHelp();
	exit(0);
      }
      i += 2;
    } else {
      if( retVal == NULL ) {
	retVal = makeSubsuiteNode( argv[i], nodeNum );
	pNode = retVal;
      } else {
	pNode->next = makeSubsuiteNode( argv[i], nodeNum );
	pNode->next->prev = pNode;
	pNode = pNode->next;
      }
      nodeNum++;
      i++;
    }
  }

  /* If no paths were specified, scan the current directory. */
  if( bPathSpecified == 0 ) {
    scanPath(".", bRecurse);
  }

  return retVal;
}

/*! \brief Returns a string representation of the specified value.
 *
 * All three parameters should be of the same value. Figures out which
 * format best matches the type for the specified value and returns a
 * string representation of it.
 */
char* stringifyValue( LARGEST_INT_TYPE val_int,
		      LARGEST_UINT_TYPE val_uint,
		      LARGEST_FLOAT_TYPE val_flt ) {

  static char buf[ 64 ];
  
  /* Figure out positive or negative by looking at the float version */
  if( 0 <= val_flt ) {
    /* Figure out if we should use the floating point version or the integer */
    /* version */
    LARGEST_FLOAT_TYPE tmp = val_flt - val_uint;
    if( (-1 < tmp && tmp < -1) &&
	( tmp < -0.000000000000001 && 0.000000000000001 < tmp ) ) {
      /* Use floating point version */
      snprintf( &buf[0], 64, "%e", val_flt );
    } else {
      /* Figure out if we can fit in a 32bit value */
      if( val_flt <= 2147483648 ) {
	/* Use the unsigned int version directly */
	snprintf( &buf[0], 64, "%u", val_uint );
      } else {
	/* Split into two 32bit values */
	snprintf( &buf[0], 64, "%p", val_uint);
      }
    }
  } else {
    /* Figure out if we should use the floating point version or the integer */
    /* version */
    LARGEST_FLOAT_TYPE tmp = val_flt - val_int;
    if( (-1 < tmp && tmp < -1) &&
	( tmp < -0.000000000000001 && 0.000000000000001 < tmp ) ) {
      /* Use floating point version */
      snprintf( &buf[0], 64, "%e", val_flt );
    } else {
      /* Figure out if we can fit in a 32bit value */
      if( val_flt <= 2147483648 ) {
	/* Use the unsigned int version directly */
	snprintf( &buf[0], 64, "%d", val_int );
      } else {
	/* Split into two 32bit values */
	snprintf( &buf[0], 64, "-%p", (val_int * -1) );
      }
    }
  }

  return &buf[0];
}

SCOOT_FN char* scoot_stringify_compare( LARGEST_INT_TYPE int_lhs,
					LARGEST_UINT_TYPE uint_lhs,
					LARGEST_FLOAT_TYPE flt_lhs,
					const char* opStr,
					LARGEST_INT_TYPE int_rhs,
					LARGEST_UINT_TYPE uint_rhs,
					LARGEST_FLOAT_TYPE flt_rhs ) {
  static char buf[ 512 ];
  int len = snprintf( &buf[0], 512, "%s %s",
		      stringifyValue( int_lhs, uint_lhs, flt_lhs ), opStr );
  
  snprintf( &buf[ len ], 512 - len, " %s",
	    stringifyValue( int_rhs, uint_rhs, flt_rhs ) );

  return &buf[0];
}

SCOOT_FN void doVersion() {
  printf("scoot (SCOOT) version %0d\n", SCOOT_VERSION);
  printf("Copyright (C) %0d Jeff Backus.\n", SCOOT_COPYRIGHT_YEAR);
  printf("This is free software, distributed under the MIT license.\n");
  printf("There is NO warranty. Refer to the source for license information.\n");
  printf("Official source code available at: %s\n", SCOOT_URL);
  printf("\n");
}

SCOOT_FN void doHelp() {
  printf("Not implemented.\n");
}

