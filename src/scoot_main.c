/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Jeff Backus <jeff@jsbackus.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*! \file
 * SCOOT Main entry point
 */

#include <string.h>
#include "scoot_internal.h"

int main( int argc, char** argv ) {

  int i;
  scoot_results_t results;
  scoot_subsuite_node_t* nodes = NULL;
  scoot_subsuite_node_t* pNode = NULL;

  scoot_start_log_file( NULL );
  scoot_start_results_file( NULL );
  
  nodes = scoot_parse_commandline( argc, argv );
  
  results.numExec = 0;
  results.numPass = 0;
  
  pNode = nodes;
  while( pNode != NULL ) {
    scoot_suite_node_t* mainNode = findSuiteNodeByName( pNode->name );

    char buf[256];
    snprintf(&buf[0], 256, "%0d", pNode->id);
    scoot_results_t* tmpResults = runSuite( mainNode, &buf[0], NULL, NULL );
    updateResults( &results, tmpResults );    

    pNode = pNode->next;
  }
  printSummary( &results );

  scoot_scan_unused();

  scoot_close_files();

  if( results.numPass != results.numExec ) {
    return 1;
  }
  return 0;
}
