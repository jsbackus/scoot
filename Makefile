####
# The MIT License (MIT)
# 
# Copyright (c) 2017 Jeff Backus <jeff@jsbackus.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
####

###
# Makefile to compile SCOOT executable.
###

## Install Target Locations
PREFIX ?= /usr/local
INCLUDEDIR ?= $(PREFIX)/include
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man
DOCDIR ?= $(PREFIX)/share/doc
## End Install Target Locations

SRCDIR=src
# List of source files
SRC=scoot.c \
	scoot_posix.c \
	scoot_win.c
MAIN_SRC=scoot_main.c

# List of header files to install
HDR=include/scoot.h

# List of documentation files to install
DOCS=README \
	LICENSE

# List of manpages to install
MANDOCS=scoot.1

OBJDIR=compiled_objs
OUTDIR=bin

# Autogenerate list of objects
OBJ=$(notdir $(SRC:.c=.o)) $(notdir $(MAIN_SRC:.c=.o))

COMPILED_OBJS=$(addprefix $(OBJDIR)/,$(OBJ))

# Target binary
TARGET=scoot

## Unit Tests
TEST_SRC=unit_tests/BasicTestsC.c \
	unit_tests/BasicTestsCPP.cpp \
	unit_tests/BasicSuite.c \
	unit_tests/TestTop.c \
	unit_tests/CyclicDependencies.c \
	unit_tests/Subsuites.c \
	unit_tests/TestOrder.c \
	unit_tests/SuiteOrder.c \
	unit_tests/SuiteOrderA.c \
	unit_tests/SuiteOrderC.c \
	unit_tests/SuiteOrderG.c \
	unit_tests/SuiteOrderH.c \
	unit_tests/SuiteOrderP.c \
	unit_tests/SuiteOrderQ.c \
	unit_tests/SuiteOrderX.c \
	unit_tests/MisnamedSuite.c \
	unit_tests/TestDefaultFns.c \
	unit_tests/SuiteSetupFail.c \
	unit_tests/SuiteCleanupFail.c \
	unit_tests/UnreferencedSuite.c \
	unit_tests/TestLogLevel.c

#CC=g++
C_COMPILER=gcc
CPP_COMPILER=g++
LINKER=g++

MACHINE_TRIPLET=$(shell $(C_COMPILER) -dumpmachine)
IS_LINUX ?= $(findstring linux,$(MACHINE_TRIPLET))
ifneq (,$(IS_LINUX))
	IS_WIN ?=
else
	IS_WIN ?= 1
endif

TEST_C_OBJS=$(addprefix $(OBJDIR)/,$(notdir $(patsubst %.c,%.scoot, $(filter %.c, $(TEST_SRC)))))
TEST_CPP_OBJS=$(addprefix $(OBJDIR)/,$(notdir $(patsubst %.cpp,%.scoot, $(filter %.cpp, $(TEST_SRC)))))
TEST_OBJS=$(TEST_C_OBJS) $(TEST_CPP_OBJS)
TEST_TOP=TestTop

TEST_LIB_SRC=unit_tests/mylibC.c \
	unit_tests/mylibCPP.cpp
TEST_LIB_C_OBJS=$(addprefix $(OBJDIR)/,$(notdir $(patsubst %.c,%.o, $(filter %.c, $(TEST_LIB_SRC)))))
TEST_LIB_CPP_OBJS=$(addprefix $(OBJDIR)/,$(notdir $(patsubst %.cpp,%.o, $(filter %.cpp, $(TEST_LIB_SRC)))))
TEST_LIB_OBJS=$(TEST_LIB_C_OBJS) $(TEST_LIB_CPP_OBJS)
TEST_LIB=mylib
ifneq (,$(IS_LINUX))
	TEST_LIB_FULL=$(OBJDIR)/lib$(TEST_LIB).so
else
	TEST_LIB_FULL=$(OBJDIR)/$(TEST_LIB).dll
endif

COMMON_CFLAGS=-Wall -O3 -Iinclude $(MISC_FLAGS)
CPPFLAGS=$(COMMON_CFLAGS) -std=c++11
CFLAGS=$(COMMON_CFLAGS) -std=c89

ifneq (,$(IS_LINUX))
	COMMON_TEST_CFLAGS=-shared -fPIC
else
	COMMON_TEST_CFLAGS=-shared
endif

LDFLAGS=
LDFLAGS_TEST=$(LDFLAGS)
ifneq (,$(IS_LINUX))
	LDFLAGS_MAIN=$(LDFLAGS) -ldl
	COMMON_TEST_LDFLAGS=-shared -fPIC -L$(PWD)/$(OBJDIR) -l$(TEST_LIB) -Wl,-rpath,$(PWD)/$(OBJDIR)
else
	LDFLAGS_MAIN=$(LDFLAGS)
	COMMON_TEST_LDFLAGS=-shared -L$(PWD)/$(OBJDIR) -l$(TEST_LIB) -Wl,-rpath,$(PWD)/$(OBJDIR)
endif
RM=rm

VPATH=$(PWD)/unit_tests

.PHONY : all clean test $(OUTDIR) $(OBJDIR) $(TARGET) install uninstall
all: $(TARGET)


install: $(TARGET)
	# Binary
	install -pD $(OUTDIR)/$(TARGET) $(BINDIR)/$(TARGET)
	# Header
	install -pD --target-directory=$(INCLUDEDIR)/ $(HDR) 
	# Documentation
	install -pD --target-directory=$(DOCDIR)/scoot/ $(DOCS)
	# Man Pages
	#install -p --target-directory=$(MANDIR)/man1 $(MANDOCS) 

uninstall:
	# Binary
	-rm $(BINDIR)/$(TARGET)
	# Header
	-rm $(addprefix $(INCLUDEDIR)/, $(notdir $(HDR)))
	# Documentation
	-rm $(addprefix $(DOCDIR)/scoot/, $(DOCS))
	-rmdir $(DOCDIR)/scoot/
	# Man Pages
	-rm $(addprefix $(MANDIR)/man1/, $(MANDOCS))

$(TEST_CPP_OBJS): $(OBJDIR)/%.scoot: %.cpp $(TEST_LIB_FULL) |$(OBJDIR)
	$(CPP_COMPILER) $(CPPFLAGS) $(COMMON_TEST_LDFLAGS) -o $@ $<

$(TEST_C_OBJS): $(OBJDIR)/%.scoot: %.c $(TEST_LIB_FULL) |$(OBJDIR)
	$(C_COMPILER) $(CFLAGS) $(COMMON_TEST_LDFLAGS) -o $@ $<

$(TEST_LIB_CPP_OBJS): $(OBJDIR)/%.o: %.cpp |$(OBJDIR)
	$(CPP_COMPILER) $(CPPFLAGS) $(COMMON_TEST_CFLAGS) -o $@ -c $<

$(TEST_LIB_C_OBJS): $(OBJDIR)/%.o: %.c |$(OBJDIR)
	$(C_COMPILER) $(CFLAGS) $(COMMON_TEST_CFLAGS) -o $@ -c $<

$(TEST_LIB): $(TEST_LIB_FULL)

$(TEST_LIB_FULL): $(TEST_LIB_OBJS)
	$(LINKER) -shared -fPIC $(TEST_LIB_OBJS) $(LDFLAGS_TEST) -o $@

$(COMPILED_OBJS): $(OBJDIR)/%.o: $(SRCDIR)/%.c |$(OBJDIR)
	$(C_COMPILER) $(CFLAGS) -o $@ -c $<

test: $(OUTDIR)/$(TARGET) $(TEST_OBJS)
	cd $(OBJDIR) && ../$(OUTDIR)/$(TARGET) $(TEST_TOP) 

$(TARGET):  $(OUTDIR)/$(TARGET)

$(OUTDIR)/$(TARGET): $(COMPILED_OBJS) |$(OUTDIR)
	$(LINKER) $(COMPILED_OBJS) $(LDFLAGS_MAIN) -o $@

clean:
	-$(RM) $(COMPILED_OBJS) core $(OUTDIR)/$(TARGET) *~
	-$(RM) $(OUTDIR)/$(TEST_TARGET) $(OBJDIR)/*

$(OBJDIR):
	mkdir -p $@

$(OUTDIR):
	mkdir -p $@

